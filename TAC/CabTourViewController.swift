//
//  CabTourViewController.swift
//  TAC
//
//  Created by prk on 29/10/17.
//  Copyright © 2017 Diaprix. All rights reserved.
//

import UIKit

class CabTourViewController: UIViewController,UIPageViewControllerDelegate,UIPageViewControllerDataSource {

    var tourPageViewController:UIPageViewController!
    var firstSlide:CityCheapestRideViewController!
    var secondSlide:UIViewController!
    var thirdSlide:UIViewController!
    var fourthSlide:UIViewController!
    var tourViewControllersList: [UIViewController] = []
    
    @IBOutlet weak var pageControl: UIPageControl!
   
    func loadViewControllers(){
    firstSlide = CityCheapestRideViewController(nibName: "CityCheapestRideViewController", bundle: nil)
        
        
    if(firstSlide != nil) {
    tourViewControllersList.append(firstSlide)
    }
    secondSlide = ShareRideDetailsViewController(nibName: "ShareRideDetailsViewController", bundle: nil)
    if(secondSlide != nil) {
    tourViewControllersList.append(secondSlide)
    }
    thirdSlide = PickYourPreferenceViewController(nibName: "PickYourPreferenceViewController", bundle: nil)
    if(thirdSlide != nil) {
    tourViewControllersList.append(thirdSlide)
    }
    fourthSlide = RideSafetyViewController(nibName: "RideSafetyViewController", bundle: nil)
    if(fourthSlide != nil) {
    tourViewControllersList.append(fourthSlide)
    }

    }
    

    
    func onSlideChanged(not:NSNotification)  {
    
        let inf = not.userInfo! as NSDictionary
        let dir = inf.value(forKey: "direction") as! String
        let currentClass = inf.value(forKey: "class") as! String
        if dir == "next"{
            
            // check class and do accordingly
            
            if currentClass ==  "CityCheapestRideViewController"{
                let vc = [tourViewControllersList[1]]
                tourPageViewController.setViewControllers(vc, direction: .forward, animated: true, completion: nil)
                pageControl.currentPage = 1
            }else if currentClass == "ShareRideDetailsViewController"{
                let vc = [tourViewControllersList[2]]
                tourPageViewController.setViewControllers(vc, direction: .forward, animated: true, completion: nil)
                pageControl.currentPage = 2
            }else if currentClass == "PickYourPreferenceViewController"{
                let vc = [tourViewControllersList[3]]
                tourPageViewController.setViewControllers(vc, direction: .forward, animated: true, completion: nil)
                pageControl.currentPage = 3
            }else if currentClass == "RideSafetyViewController"{
                // dismiss tour
            }
            
        }else{
            
            if currentClass ==  "CityCheapestRideViewController"{
                
            }else if currentClass == "ShareRideDetailsViewController"{
                
                let vc = [tourViewControllersList[0]]
                tourPageViewController.setViewControllers(vc, direction: .reverse, animated: true, completion: nil)
                pageControl.currentPage = 0
                
            }else if currentClass == "PickYourPreferenceViewController"{
                
                let vc = [tourViewControllersList[1]]
                tourPageViewController.setViewControllers(vc, direction: .reverse, animated: true, completion: nil)
                pageControl.currentPage = 1
                
            }else if currentClass == "RideSafetyViewController"{
                
                let vc = [tourViewControllersList[2]]
                tourPageViewController.setViewControllers(vc, direction: .reverse, animated: true, completion: nil)
                pageControl.currentPage = 2
            }

        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        NotificationCenter.default.addObserver(self, selector: #selector(CabTourViewController.onSlideChanged(not:)), name: NSNotification.Name(rawValue: "dummy"), object: nil)
        
        
        tourPageViewController = self.storyboard?.instantiateViewController(withIdentifier: "PageVIewControllerID") as! UIPageViewController
        
        self.loadViewControllers()
        // Adding tourPageViewController datasource & delegates
        tourPageViewController.dataSource = self
        tourPageViewController.delegate = self 
        // Setting Pagecontrol attributes
        
        pageControl.pageIndicatorTintColor = UIColor.lightGray
        pageControl.currentPageIndicatorTintColor = UIColor.black
        pageControl.numberOfPages = 4
        pageControl.currentPage = 0
        
        tourPageViewController.setViewControllers([tourViewControllersList[0]], direction: UIPageViewControllerNavigationDirection.forward, animated: true, completion: nil)
        tourPageViewController.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height-70)
        self.addChildViewController(tourPageViewController)
        self.view.addSubview(tourPageViewController.view)
        self.tourPageViewController.didMove(toParentViewController: self)
        //disable interaction as it is not implemented completely.
        pageControl.isUserInteractionEnabled = false

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?{
        
        var prevViewController:UIViewController? = nil
        
        if viewController == fourthSlide {
//            pageControl.currentPage = 2
            prevViewController = thirdSlide
            
        }
        if viewController == thirdSlide {
//            pageControl.currentPage = 1
            prevViewController = secondSlide
            
        }
        if viewController == secondSlide {
//            pageControl.currentPage = 0
            prevViewController = firstSlide
            
        }
        return prevViewController
    }
    
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        
        var nextViewController:UIViewController? = nil
        
        if viewController == firstSlide {
//            pageControl.currentPage = 1
            nextViewController = secondSlide
            
        }
        if viewController == secondSlide {
//            pageControl.currentPage = 2
            nextViewController = thirdSlide
            
        }
        if viewController == thirdSlide {
//            pageControl.currentPage = 3
            nextViewController = fourthSlide
            
        }else{
            // dismiss tour view controller
        }
        
        
        return nextViewController
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if (!completed)
        {
            
            print("Error completing Transition")
            return;
        }
        
//        if accountType.isEqual(to: self.accountTypehumFree ) {
            if pageViewController.viewControllers?.last is CityCheapestRideViewController {
                pageControl.currentPage = 0
            }else if pageViewController.viewControllers?.last is ShareRideDetailsViewController {
                pageControl.currentPage = 1
            }else if pageViewController.viewControllers?.last is PickYourPreferenceViewController {
                pageControl.currentPage = 2
            }else if pageViewController.viewControllers?.last is RideSafetyViewController {
                pageControl.currentPage = 3
            }
//        }else if accountType.isEqual(to: self.accountTypehumPlus ){
//            if pageViewController.viewControllers?.last is AutoHealthTourCard {
//                pageControl.currentPage = 0
//                endTourButton.isHidden = false; //optional
//            }else if pageViewController.viewControllers?.last is EmergencyAssistanceTourCard {
//                pageControl.currentPage = 1
//                endTourButton.isHidden = false; //optional
//            }else if pageViewController.viewControllers?.last is VehicleLocationTourCard {
//                pageControl.currentPage = 2
//                endTourButton.isHidden = false; //optional
//            }else if pageViewController.viewControllers?.last is SafetyScoreTourCard {
//                pageControl.currentPage = 3
//                endTourButton.isHidden = false;
//            }else if pageViewController.viewControllers?.last is ExitTourCard {
//                pageControl.currentPage = 4
//                endTourButton.isHidden = true;
//            }
//        }else if accountType.isEqual(to: self.accountTypehumX ) {
//            if pageViewController.viewControllers?.last is WifiHotspotTourCard {
//                pageControl.currentPage = 0
//            }else if pageViewController.viewControllers?.last is AutoHealthTourCard {
//                pageControl.currentPage = 1
//                endTourButton.isHidden = false; //optional
//            }else if pageViewController.viewControllers?.last is EmergencyAssistanceTourCard {
//                pageControl.currentPage = 2
//                endTourButton.isHidden = false; //optional
//            }else if pageViewController.viewControllers?.last is VehicleLocationTourCard {
//                pageControl.currentPage = 3
//                endTourButton.isHidden = false; //optional
//            }else if pageViewController.viewControllers?.last is SafetyScoreTourCard {
//                pageControl.currentPage = 4
//                //display the end button as it will be hidden in end tour screen
//                endTourButton.isHidden = false;
//            }else if pageViewController.viewControllers?.last is ExitTourCard {
//                pageControl.currentPage = 5
//                endTourButton.isHidden = true;
//            }
//            
//        }else if accountType.isEqual(to: self.accountTypehumFreeUpsell ) {
//            if pageViewController.viewControllers?.last is WifiHotspotTourCard {
//                pageControl.currentPage = 0
//            }else if pageViewController.viewControllers?.last is AutoHealthTourCard {
//                pageControl.currentPage = 1
//                endTourButton.isHidden = false; //optional
//            }else if pageViewController.viewControllers?.last is EmergencyAssistanceTourCard {
//                pageControl.currentPage = 2
//                endTourButton.isHidden = false; //optional
//            }else if pageViewController.viewControllers?.last is VehicleLocationTourCard {
//                pageControl.currentPage = 3
//                endTourButton.isHidden = false; //optional
//            }else if pageViewController.viewControllers?.last is UpgradeNowTourCard {
//                pageControl.currentPage = 4
//                //display the end button as it will be hidden in end tour screen
//                endTourButton.isHidden = false;
//            }
//            //TODO add tag on load - this needs to be replaced with SWIPE direction - SWIPE_LEFT_FREE or SWIPE_RIGHT_FREE
//            if let tag = adobeTagPrefixList[pageControl.currentPage] {
//                AdobeAnalytics.sharedInstance().adobeTrackingAction(tagName: tag + VIEW_PAGE_FREE)
//            }
        }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
