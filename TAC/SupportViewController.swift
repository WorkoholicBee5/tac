//
//  SupportViewController.swift
//  TAC
//
//  Created by prk on 31/10/17.
//  Copyright © 2017 Diaprix. All rights reserved.
//

import UIKit
import CDAlertView

class SupportViewController: UIViewController {

    @IBOutlet weak var activityController: UIActivityIndicatorView!
    @IBOutlet weak var supportTextView: UITextView!
    @IBOutlet weak var descriptionLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        descriptionLabel.text = "Thanks for your interset in TAC. Please use this form\nif you have any queries or questions about our\nservice and we'll get back very soon."
     
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onSubmitTapped(_ sender: Any) {
           self.submitCompliant()
    }
    @IBAction func onCancelTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
        func displayAlertWithMessage(message:String, title:String,imagename:String,buttonName:String) {
            
            
            let alert = CDAlertView(title: title, message: message, type: .custom(image: UIImage(named:imagename)!))
            
            let doneAction = CDAlertViewAction(title: buttonName)
            alert.add(action: doneAction)
            
            alert.show()
//        let aletController = UIAlertController(title: title, message: message, preferredStyle: .alert)
////        let okAction = UIAlertAction(title: "ok", style: .default, handler: nil)
//        let okAction = UIAlertAction(title: "ok", style: .default) { (alert) in
//            self.dismiss(animated: true, completion: nil)
//        }
//        aletController.addAction(okAction)
//        self.present(aletController, animated: true, completion: nil)
        
    }
    
    
    func submitCompliant() {
        
        activityController.startAnimating()
        
        let userId = UserDefaults.standard.value(forKey: "userid") as! String
        
        
        let headers = [
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            "postman-token": "71d49460-d134-eda6-db6d-45f2bd140d00"
        ]
        
        let postData = NSMutableData(data: "message=\(self.supportTextView.text)".data(using: String.Encoding.utf8)!)
        postData.append("&customerId=\(userId)".data(using: String.Encoding.utf8)!)
        
        let request = NSMutableURLRequest(url: NSURL(string: "http://139.59.71.224:3010/api/Supports")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                
                DispatchQueue.main.async {
                    self.activityController.stopAnimating()
                }
                print(error)
            } else {
                let httpResponse = response as? HTTPURLResponse
                print(httpResponse)
                DispatchQueue.main.async {
                    self.activityController.stopAnimating()
                    //self.displayAlertWithMessage(message: "complaint sent successfully", title: "")
                    self.displayAlertWithMessage(message: "complaint sent successfully", title: "Success", imagename: "success", buttonName: "OK")
                }
                
                
            }
        })
        dataTask.resume()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
