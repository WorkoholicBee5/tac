//
//  SearchPlacesViewController.swift
//  TAC
//
//  Created by Kshiteej deshpande on 12/12/17.
//  Copyright © 2017 Verizon. All rights reserved.
//

import UIKit

class SearchPlacesViewController: UIViewController,UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {

    @IBOutlet var backButton: UIButton!
    
    var lblNodata = UILabel()

    @IBOutlet var tblLoction: UITableView!
    var arrPlaces = NSMutableArray(capacity: 100)
    let operationQueue = OperationQueue()

    var state : String!
    
    var latitude : NSNumber!
    var longitute : NSNumber!
    
    var userObj : objects!

    var dataobj : DataObj!
    
    
   // var LocationDataDelegate : LocationData! = nil
    @IBOutlet var searchTextField: UITextField!
    @IBOutlet var searchText: UISearchBar!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lblNodata.frame = CGRect(x: 0, y: 80, width:
            self.view.frame.size.width, height: self.view.frame.size.height-60)
        lblNodata.text = "Please enter text to get your location"
        self.view.addSubview(lblNodata)
        //srchLocation.placeholder = "Ente your location details"
        lblNodata.textAlignment = .center
        
      
        
    }
    @IBAction func backButton(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)

    }
        func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.beginSearching(searchText: searchText)
    }

//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
//    {
//        self.beginSearching(searchText: searchTextField.text!)
//
//        return true
//
//    }
//
    func beginSearching(searchText:String) {
        if searchText.characters.count == 0 {
            self.arrPlaces.removeAllObjects()
            tblLoction.isHidden = true
            lblNodata.isHidden = false
            return
        }
        
        DispatchQueue.main.async {
            self.forwardGeoCoding(searchText: searchText)
        }
    }
    
    //MARK: - Search place from Google -
    func forwardGeoCoding(searchText:String) {
        googlePlacesResult(input: searchText) { (result) -> Void in
            let searchResult:NSDictionary = ["keyword":searchText,"results":result]
            
            print("result",result)
            if result.count > 0
            {
                let features = searchResult.value(forKey: "results") as! NSArray
                self.arrPlaces = NSMutableArray(capacity: 100)
                print(features.count)
                for jk in 0...features.count-1
                {
                    let dict = features.object(at: jk) as! NSDictionary
                    self.arrPlaces.add(dict)
                }
                DispatchQueue.main.async(execute: {
                    if self.arrPlaces.count != 0
                    {
                        self.tblLoction.isHidden = false
                        self.lblNodata.isHidden = true
                        self.tblLoction.reloadData()
                    }
                    else
                    {
                        self.tblLoction.isHidden = true
                        self.lblNodata.isHidden = false
                        self.tblLoction.reloadData()
                    }
                });
            }
        }
    }
    
    //MARK: - Google place API request -
    func googlePlacesResult(input: String, completion: @escaping (_ result: NSArray) -> Void) {
        let searchWordProtection = input.replacingOccurrences(of: " ", with: "");        if searchWordProtection.characters.count != 0 {
            
            let currentLocationLatitude = 12.453463634545
            let currentLocationLongtitude = 77.2342544643545435
            
            let urlString = NSString(format: "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&types=establishment|geocode&location=%@,%@&radius=500&language=en&key=AIzaSyDvnSkHURaHrAe8RaB0S4NYxrzOCZT-qpU",input,"\(currentLocationLatitude)","\(currentLocationLongtitude)")
            print(urlString)
            let url = NSURL(string: urlString.addingPercentEscapes(using: String.Encoding.utf8.rawValue)!)
            print(url!)
            let defaultConfigObject = URLSessionConfiguration.default
            let delegateFreeSession = URLSession(configuration: defaultConfigObject, delegate: nil, delegateQueue: OperationQueue.main)
            let request = NSURLRequest(url: url! as URL)
            let task =  delegateFreeSession.dataTask(with: request as URLRequest, completionHandler:
            {
                (data, response, error) -> Void in
                if let data = data
                {
                    do {
                        let jSONresult = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]
                        let results:NSArray = jSONresult["predictions"] as! NSArray
                        let status = jSONresult["status"] as! String
                        if status == "NOT_FOUND" || status == "REQUEST_DENIED"
                        {
                            let userInfo:NSDictionary = ["error": jSONresult["status"]!]
                            let newError = NSError(domain: "API Error", code: 666, userInfo: userInfo as [NSObject : AnyObject])
                            let arr:NSArray = [newError]
                            completion(arr)
                            return
                        }
                        else
                        {
                            completion(results)
                        }
                    }
                    catch
                    {
                        print("json error: \(error)")
                    }
                }
                else if let error = error
                {
                    print(error)
                }
            })
            task.resume()
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPlaces.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let tblCell = tableView.dequeueReusableCell(withIdentifier: "locationCell")
        let dict = arrPlaces.object(at: indexPath.row) as! NSDictionary
        tblCell?.textLabel?.text = dict.value(forKey: "description") as? String
        tblCell?.textLabel?.numberOfLines = 0
        tblCell?.textLabel?.sizeToFit()
        return tblCell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
      //  if LocationDataDelegate != nil
      //  {
            let dict = arrPlaces.object(at: indexPath.row) as! NSDictionary
            print(dict.value(forKey: "terms") as! NSArray)
            let ArrSelected = dict.value(forKey: "terms") as! NSArray
        print("dict",dict)
        print("ArrSelected",ArrSelected)
        
        let description = dict.value(forKey: "description")
        
        print("description",description!)
      //      LocationDataDelegate.didSelectLocationData(LocationData: ArrSelected)
     //   }
        
        let placeid = dict.value(forKey: "place_id")
        
        places(placeId: placeid as! String,description: description as! String)
        
      //  self.dismiss(animated: true, completion: nil)
    }
    
    func places(placeId:String,description:String)
    {
        
        let headers = [
            "cache-control": "no-cache",
            "postman-token": "238a405c-eaf6-a2b2-9b01-4306a824ca14"
        ]
        
        let urlString = NSString(format: "https://maps.googleapis.com/maps/api/place/details/json?placeid=\(placeId)&key=AIzaSyDvnSkHURaHrAe8RaB0S4NYxrzOCZT-qpU" as NSString)
        print(urlString)
        
        //  print("urlstr",urlstr)
        
        let request = NSMutableURLRequest(url: NSURL(string: urlString as String)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error)
                DispatchQueue.main.async {
                    
                        self.displayAlertWithMessage(message: "getting Error From Server", title: "Network Error", imagename: "error", buttonName: "OK")
                }
            } else {
                let httpResponse = response as? HTTPURLResponse
                print(httpResponse!)
                do{
                    let result = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    
                    print("results",result)

                    let lat = ((((result.value(forKey: "result") as! NSDictionary).value(forKey: "geometry") as! NSDictionary) ).value(forKey: "location") as! NSDictionary).value(forKey: "lat") as! NSNumber
                    
                    let lng = ((((result.value(forKey: "result") as! NSDictionary).value(forKey: "geometry") as! NSDictionary) ).value(forKey: "location") as! NSDictionary).value(forKey: "lng") as! NSNumber
                    
                    print("lat",lat,lng)
                    
                    self.latitude = lat
                    self.longitute = lng
                    
                    
                  //  let vc =  self.storyboard?.instantiateViewController(withIdentifier: "FirstViewController") as? FirstViewController
                    
//                    vc?.statustxt = self.state
//
//                    vc?.lat = self.latitude
//
//                    vc?.lng = self.longitute
//
//                    vc?.descriptiontxt = description

                        UserDefaults.standard.setValue(self.latitude, forKey: "lat")
                        UserDefaults.standard.setValue(self.longitute, forKey: "lng")
                        UserDefaults.standard.setValue(description, forKey: "descriptiontxt")
                        
                  //  self.dismiss(animated: true, completion: nil)
                    DispatchQueue.main.async {

                       // self.presentingViewController?.dismiss(animated: true, completion: nil)
                        self.dismiss(animated: true, completion: nil)
                    }
                }
            }
            
        })
        
        dataTask.resume()
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)  {
        let something = segue.destination as! FirstViewController
       // something.aVariable = anotherVariable
    }
    func displayAlertWithMessage(message:String, title:String,imagename:String,buttonName:String) {
        
        
        //        let alert = CDAlertView(title: title, message: message, type: .custom(image: UIImage(named:imagename)!))
        //
        //        let doneAction = CDAlertViewAction(title: buttonName)
        //        alert.add(action: doneAction)
        //
        //        alert.show()
        
        let aletController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "ok", style: .default, handler: nil)
        aletController.addAction(okAction)
        self.present(aletController, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
