//
//  LoginViewController.swift
//  TAC
//
//  Created by prk on 25/10/17.
//  Copyright © 2017 Diaprix. All rights reserved.
//

import UIKit
import CDAlertView

class LoginViewController: UIViewController,UITextFieldDelegate {
    var otpDetails:NSDictionary!

    @IBOutlet weak var activityController: UIActivityIndicatorView!
    @IBOutlet weak var mobileTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "splash")!)
        
        self.mobileTF.delegate = self
        
//        self.sendOtpToMobileNumber(mobile: 7795729526)
//        self.validateCustomer()
      
        
    
    }
    func textFieldShouldReturn(userText: UITextField!) -> Bool {
        mobileTF.resignFirstResponder()
        return true;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func validate(value: String) -> Bool {

        let PHONE_REGEX = "^[789]\\d{9}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    
    @IBAction func onLoginTapped(_ sender: Any) {
    
        if self.validate(value: mobileTF.text!) {

        self.sendOtpToMobileNumber(mobile: Double(self.mobileTF.text!)!)
            
        }else{
            DispatchQueue.main.async {
                
          //  self.displayAlertWithMessage(message: "Please enter valid mobile number", title: "Error...!")
                
                self.displayAlertWithMessage(message: "Please enter valid mobile number", title: "Error...!", imagename: "alert", buttonName: "OK")
                }
        }
        

    }

   
    
    func sendOtpToMobileNumber(mobile:Double)   {
        
        self.activityController.startAnimating()
        let headers = [
            "content-type": "application/json",
            "cache-control": "no-cache",
            "postman-token": "b572d9c6-9183-785c-73c9-4450fad35a5d"
        ]
        let parameters = ["details": ["mobileNo": mobile]] as [String : Any]
        
        
        var postData:Data!
        do {
         postData =  try JSONSerialization.data(withJSONObject: parameters, options: [])
        }catch {
            
        }
        
        let request = NSMutableURLRequest(url: NSURL(string: "http://139.59.71.224:3010/api/Customers/sendOtpToCustomer")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                DispatchQueue.main.async {
                self.activityController.stopAnimating()
                   // self.displayAlertWithMessage(message: "unable to send OTP", title: "")
                self.displayAlertWithMessage(message: "unable to send OTP", title: "Error", imagename: "error", buttonName: "OK")
                }
            } else {
                
                do{
                let result = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as! NSDictionary
                    DispatchQueue.main.async {
                    UserDefaults.standard.setValue(mobile, forKey: "mobile")
                    UserDefaults.standard.synchronize()
                    
                        print("Loginresult",result)
                    
                        self.activityController.stopAnimating()
                    self.mobileTF.text = nil
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OTPViewControllerID") as! OTPViewController
                        vc.otpDict = result
                        self.present(vc, animated: true, completion: nil)
                        }
                    
                    
                }catch{
                    DispatchQueue.main.async {
                        self.activityController.stopAnimating()
                      //  self.displayAlertWithMessage(message: "unable to send OTP", title: "")
                        
                        self.displayAlertWithMessage(message: "unable to send OTP", title: "OTP", imagename: "error", buttonName: "OK")
                    }
                }
                
                
            }
        })
        
        dataTask.resume()
    
    }
    
    func displayAlertWithMessage(message:String, title:String,imagename:String,buttonName:String) {

        let alert = CDAlertView(title: title, message: message, type: .custom(image: UIImage(named:imagename)!))
        
        let doneAction = CDAlertViewAction(title: buttonName)
        alert.add(action: doneAction)
        
        alert.show()
        
//        let aletController = UIAlertController(title: title, message: message, preferredStyle: .alert)
//        let okAction = UIAlertAction(title: "ok", style: .default, handler: nil)
//        aletController.addAction(okAction)
//        self.present(aletController, animated: true, completion: nil)
    
    }
    
    
}
