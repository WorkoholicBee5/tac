//
//  RegisterViewController.swift
//  TAC
//
//  Created by prk on 25/10/17.
//  Copyright © 2017 Diaprix. All rights reserved.
//

import UIKit
import CDAlertView

class RegisterViewController: UIViewController {
    @IBOutlet weak var activityController: UIActivityIndicatorView!

    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var mobileNumber: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "splash")!)
        
        let mobile = UserDefaults.standard.value(forKey: "mobile") as! Int
        self.mobileNumber.text = "\(mobile)"
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onCancelButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func onRegisterTapped(_ sender: Any) {
        self.ReisterUser(name: self.nameTF.text!)
    }
    
    func ReisterUser(name:String){
        
        self.activityController.startAnimating()
        let headers = [
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            "postman-token": "4e0fcd44-2c56-e0f1-79e5-20ccb38087f3"
        ]
        
        let nameStr = self.nameTF.text
        
        
        let postData = NSMutableData(data: "name=\(nameStr!)".data(using: String.Encoding.utf8)!)
        postData.append("&dob=2017-11-02T06:25:24.412Z".data(using: String.Encoding.utf8)!)
        let mobile = UserDefaults.standard.value(forKey: "mobile") as! Int
        postData.append("&mobile=\(mobile)".data(using: String.Encoding.utf8)!)
        postData.append("&password=anvi".data(using: String.Encoding.utf8)!)
        
        print("postData",postData)
        
        let request = NSMutableURLRequest(url: NSURL(string: "http://139.59.71.224:3010/api/Customers")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        print("request",request)
        
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error)
                
                DispatchQueue.main.async {
                  //  self.displayAlertWithMessage(message: "Error", title: "unable to register, try after some time")
                    
                    self.displayAlertWithMessage(message:  "unable to register, try after some time", title: "Error", imagename: "alert", buttonName: "OK")
                    self.activityController.stopAnimating()
                }
            } else {
                let httpResponse = response as? HTTPURLResponse
                print(httpResponse)
                
                do {
                    let result = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as! NSDictionary
                    
                    print("result",result)
                    
//                    UserDefaults.standard.setValue((result.object(at: 0) as! NSDictionary).object(forKey: "id")!, forKey: "userid")
                    DispatchQueue.main.async {

                    UserDefaults.standard.setValue(result.object(forKey: "id")!, forKey: "userid")

                    UserDefaults.standard.synchronize()
                    
                    UserDefaults.standard.setValue(self.nameTF.text, forKey: "name")
                    UserDefaults.standard.synchronize()
                    }
                }catch {
                    
                }
                DispatchQueue.main.async {
                    self.activityController.stopAnimating()
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SWRevealViewControllerID")
                    self.present(vc!, animated: true, completion: nil)
                }
                
            }
        })
        
        dataTask.resume()
    }
    
    func displayAlertWithMessage(message:String, title:String,imagename:String,buttonName:String) {
        
        
        let alert = CDAlertView(title: title, message: message, type: .custom(image: UIImage(named:imagename)!))
        
        let doneAction = CDAlertViewAction(title: buttonName)
        alert.add(action: doneAction)
        
        alert.show()
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
