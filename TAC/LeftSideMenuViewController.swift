//
//  LeftSideMenuViewController.swift
//  TAC
//
//  Created by prk on 14/10/17.
//  Copyright © 2017 Diaprix. All rights reserved.
//

import UIKit


class LeftSideMenuViewController: UIViewController {
    
    var ListArray:NSMutableArray!
    var ListimagesArray:NSMutableArray!

    @IBOutlet weak var itemsTableView: UITableView!
    
    @IBOutlet weak var userNameLabel: UILabel!

    @IBOutlet weak var userMobileLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        if let userName = UserDefaults.standard.value(forKey: "name"){
            self.userNameLabel.text = userName as? String
        }
        if let mNumber = UserDefaults.standard.value(forKey: "mobile"){
            self.userMobileLabel.text = mNumber as? String
        }
        itemsTableView.backgroundColor = UIColor.clear
        
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "navigatioslider")!)
    
        ListArray = ["Your Rides","Support","Emergency","Logout","Share App"]

        //ListimagesArray = ["ride_list_icon","support_new_icon","emergency_icon","logout_icon","new_share_icon",]
        
        ListimagesArray = ["ride_list_icon","support_new_icon","emergency_icon","logout_icon","new_share_icon",]
    }

    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
}
@available(iOS 9.0, *)
extension LeftSideMenuViewController:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ListArray.count;
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "identifier")
        cell.textLabel?.text = ListArray.object(at: indexPath.row) as? String
        cell.textLabel?.textColor = UIColor.white
        
        cell.imageView?.image = UIImage(named: ListimagesArray.object(at: indexPath.row) as! String)
        
        cell.backgroundColor = UIColor.clear
        return cell
    }
    @available(iOS 9.0, *)
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // to toggle between lef and center panel
        self.revealViewController().revealToggle(animated: true)
        
        if (indexPath.row == 0) {
            //                YourRidesViewControllerID
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "YourRidesViewControllerID")
            self.present(vc!, animated: true, completion: nil)
            
        }else if (indexPath.row == 1) {
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SupportViewControllerID")
            //                self.navigationController?.pushViewController(vc!, animated: true)
            self.present(vc!, animated: true, completion: nil)
            
        }else if (indexPath.row == 2) {
            
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "emergencynavID")
            //                self.navigationController?.pushViewController(vc!, animated: true)
            self.present(vc!, animated: true, completion: nil)
            
        }else if (indexPath.row == 3) {
            
            UserDefaults.standard.set(nil, forKey: "mobile")
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController")
            
            self.present(vc!, animated: true, completion: nil)
            
        }else if (indexPath.row == 4) {
            
            // share about app
            
            let textToShare = "Hello world"
            let urlToShare = NSURL(string: "http://samwize.com")
            let activityViewController = UIActivityViewController(activityItems: [textToShare, urlToShare!], applicationActivities: nil)
            // Exclude irrelevant activities
            if #available(iOS 9.0, *) {
                activityViewController.excludedActivityTypes =
                    [UIActivityType.assignToContact, UIActivityType.saveToCameraRoll, UIActivityType.postToFlickr,
                     UIActivityType.postToVimeo, UIActivityType.openInIBooks]
            } else {
                // Fallback on earlier versions
            }
            //                navigationController?.present(activityViewController, animated: true, completion: nil)
            self.present(activityViewController, animated: true, completion: nil)
        }
    }
    
}
