//
//  ContactsViewController.swift
//  TAC
//
//  Created by prk on 04/11/17.
//  Copyright © 2017 Diaprix. All rights reserved.
//

import UIKit
import CDAlertView

class ContactsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var activityController: UIActivityIndicatorView!
    
    @IBOutlet weak var contactsTableView: UITableView!
    var contactsArray = [Contacts]()

    @IBOutlet weak var contactsView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        contactsView.isHidden = true
    
        self.contactsTableView.reloadData()
        
        self.getSavedContacts()
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var mobileTF: UITextField!
    
    @IBAction func onCancelTapped(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    @IBOutlet weak var nameTF: UITextField!

    @IBAction func onAddTapped(_ sender: Any) {
        
        if self.contactsArray.count > 4 {
            //self.displayAlertWithMessage(message: "Already selected 5 contacts", title: "Oops....!")
            
            self.displayAlertWithMessage(message: "Already selected 5 contacts", title: "Oops....!", imagename: "alert", buttonName: "OK")
        }else{
            contactsView.isHidden = false
            self.nameTF.becomeFirstResponder()
        }
        
    }
    
     @IBAction func onCancelNumberTapped(_ sender: Any) {
        
        self.mobileTF.text = ""
        self.nameTF.text = ""
        self.contactsView.isHidden = true
     }
    
     @IBAction func onSaveTapped(_ sender: Any) {
        
        
        if (self.nameTF.text?.characters.count)! == 0 {
          //  self.displayAlertWithMessage(message: "Name should not be empty", title: "Warning")
            
            self.displayAlertWithMessage(message: "Name should not be empty", title: "Warning", imagename: "alert", buttonName: "Done")
        }else{
            if self.validate(value: self.mobileTF.text!) {
                
               self.addContact()               //
                
//                print(UserDefaults.standard.value(forKey: "contacts")!)
                
//                let userDefaults = UserDefaults.standard
//                userDefaults.setValue(NSKeyedArchiver.archivedData(withRootObject: contactsArray), forKey: "contacts")
//                userDefaults.synchronize()
//                
//                let array : [Contacts]
//                array = NSKeyedUnarchiver.unarchiveObject(with: (userDefaults.object(forKey: "contacts") as! NSData) as Data) as! [Contacts]
//                print("\(array[0].name)\(array[1].name)")
////                print(decodedTeams)

                
                
//                var newPerson = [Person]()
//                newPerson.append(Person(name: "Leo", age: 45))
//                newPerson.append(Person(name: "Dharmesh", age: 25))
//                let personData = NSKeyedArchiver.archivedDataWithRootObject(newPerson)
//                NSUserDefaults().setObject(personData, forKey: "personData")
//                
//                //retrive your values
//                if let loadedData = NSUserDefaults().dataForKey("personData") {
//                    loadedData
//                    if let loadedPerson = NSKeyedUnarchiver.unarchiveObjectWithData(loadedData) as? [Person] {
//                        loadedPerson[0].name   //"Leo"
//                        loadedPerson[0].age    //45
//                    }
//                }

//                var cont = [Contacts]()
//                cont.append(Contacts(name: self.nameTF.text!, number: self.mobileTF.text!))
//                
//                
//                let contData = NSKeyedArchiver.archivedData(withRootObject: cont)
//                UserDefaults.standard.set(contData, forKey: "contacts")
//                UserDefaults.standard.synchronize()
//                
//                
//                if let loadedData = UserDefaults.standard.data(forKey: "contacts"){
//                    
//                    if let loadedPerson = NSKeyedUnarchiver.unarchiveObject(with: loadedData) as? [Contacts]{
//
//                        print(loadedPerson[0].name)
//                        print(loadedPerson[0].number)
//
//                    }
//                }
//                
                
                self.contactsView.isHidden = true
                self.nameTF.text = ""
                self.mobileTF.text = ""
                self.contactsTableView.reloadData()
            
            }else{
                //self.displayAlertWithMessage(message: "Please enter valid mobile number", title: "Error...!")
                
                self.displayAlertWithMessage(message: "Please enter valid mobile number", title: "Error...!", imagename: "alert", buttonName: "alert")
            }
        }
        
        
     }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return contactsArray.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        

        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell") as! ContactsTableViewCell
        cell.nameLabel.text = contactsArray[indexPath.row].name! as String
        cell.numberLabel.text = contactsArray[indexPath.row].number! as String
        cell.contactSwitch.tag = indexPath.row
        cell.contactSwitch.addTarget(self, action: #selector(self.switchChanged(not:)), for: .valueChanged)
        if contactsArray[indexPath.row].alwaysShare {
            cell.contactSwitch.isOn = true
        }else{
            cell.contactSwitch.isOn = false
        }
        return cell
    }
    
    func switchChanged(not:UISwitch)  {
//        print(sen.isOn)
        let key = "contactid\(not.tag+1)"
        print(UserDefaults.standard.value(forKey: key)!)
        let contactID = UserDefaults.standard.value(forKey: key)!
        var alwaysValueToPass:Bool!
        if not.isOn {
            alwaysValueToPass = true
        }else{
    
            alwaysValueToPass = false
        }
    
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func validate(value: String) -> Bool {
        
        let PHONE_REGEX = "^[789]\\d{9}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    func displayAlertWithMessage(message:String, title:String,imagename:String,buttonName:String) {
        
        
        let alert = CDAlertView(title: title, message: message, type: .custom(image: UIImage(named:imagename)!))
        
        let doneAction = CDAlertViewAction(title: buttonName)
        alert.add(action: doneAction)
        
        alert.show()
        
//        let aletController = UIAlertController(title: title, message: message, preferredStyle: .alert)
//        let okAction = UIAlertAction(title: "ok", style: .default, handler: nil)
//        aletController.addAction(okAction)
//        self.present(aletController, animated: true, completion: nil)
        
    }
    
    func getSavedContacts()  {
        
        self.activityController.startAnimating()
        let headers = [
            "cache-control": "no-cache",
            "postman-token": "1195d325-872b-1e47-a3a5-118e84811ac5"
        ]
        
        
        
        let idd = UserDefaults.standard.value(forKey: "userid") as! String
        
        let request = NSMutableURLRequest(url: NSURL(string: "http://139.59.71.224:3010/api/EmergencyContacts?where=%7B%22customerId\(idd)")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
        
            } else {
                do{
                    let resuli = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as! NSArray
                      self.contactsArray.removeAll()
                    for element in resuli{
                let testDict = element as! NSDictionary
                        let cot = Contacts()
                        let contactid = testDict.object(forKey: "id") as! String
                        let contactIndex = self.contactsArray.count + 1
                        UserDefaults.standard.setValue(contactid, forKey: "contactid\(contactIndex)")
                        UserDefaults.standard.synchronize()
                        cot.name = (testDict.object(forKey: "data") as! NSDictionary).object(forKey: "name")! as! NSString
                        cot.number = (testDict.object(forKey: "data") as! NSDictionary).object(forKey: "mobileNo")! as! NSString
                        if (testDict.object(forKey: "data") as! NSDictionary).object(forKey: "alwaysShare")as! String == "no"{
                            cot.alwaysShare = false
                        }else{
                            cot.alwaysShare = true
                        }
                        self.contactsArray.append(cot)
//
                        DispatchQueue.main.async {
                            self.activityController.stopAnimating()
                            self.contactsTableView.reloadData()
                            
                        }
                    }
                }catch{
                    
                }
                
            }
        })
        
        dataTask.resume()
    }
    
    func addContact()  {
        
        self.activityController.startAnimating()
        
        let headers = [
            "content-type": "application/json",
            "cache-control": "no-cache",
            "postman-token": "54981148-c50c-becb-a7a2-32adbb42db3b"
        ]
        
        
        let idd = UserDefaults.standard.value(forKey: "userid") as! String
        let nam = self.nameTF.text!
        let parameters = ["data": [
            "name": self.nameTF.text!,
//            "name": "rrr",
            "alwaysShare": "no",
            "mobileNo": self.mobileTF.text!,
//            "mobileNo": "9999999999",
            "customerId": idd
            ]] as [String : Any]
        
        var postData:NSData!
        do{
         postData = try JSONSerialization.data(withJSONObject: parameters, options: []) as NSData
        }catch {
            
        }
        
        
        let request = NSMutableURLRequest(url: NSURL(string: "http://139.59.71.224:3010/api/EmergencyContacts")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error)
            } else {
                let httpResponse = response as? HTTPURLResponse
                print(httpResponse!)

                do{
                    let res = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as! NSDictionary
                    print(res)
                    
                }catch{
                    
                }
                DispatchQueue.main.async {
                    
                
                
                
                self.contactsTableView.reloadData()
                    self.activityController.stopAnimating()
                    
                    }
                
                
                self.getSavedContacts()
                do{
                    let result = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as! NSDictionary
                    print(result)
                }catch{
                    print("error")
                }
            }
        })
        
        dataTask.resume()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
