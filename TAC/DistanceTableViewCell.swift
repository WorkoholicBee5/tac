//
//  DistanceTableViewCell.swift
//  TAC
//
//  Created by Kshiteej deshpande on 07/12/17.
//  Copyright © 2017 Verizon. All rights reserved.
//

import UIKit

class DistanceTableViewCell: UITableViewCell {

    @IBOutlet var originLabel: UILabel!
    @IBOutlet var destinationLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
