//
//  RidesTableViewCell.swift
//  TAC
//
//  Created by prk on 01/11/17.
//  Copyright © 2017 Diaprix. All rights reserved.
//

import UIKit

class RidesTableViewCell: UITableViewCell {

    @IBOutlet weak var fareLabel: UILabel!
    @IBOutlet weak var endLocation: UILabel!
    @IBOutlet weak var endTime: UILabel!
    @IBOutlet weak var startTime: UILabel!
    @IBOutlet weak var startLoc: UILabel!
    @IBOutlet weak var endView: UIView!
    @IBOutlet weak var startView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
