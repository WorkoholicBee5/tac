//
//  YourRidesViewController.swift
//  TAC
//
//  Created by prk on 29/10/17.
//  Copyright © 2017 Diaprix. All rights reserved.
//

import UIKit

import Alamofire

class YourRidesViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var totalTripDetails:NSArray!

    var Userid : NSString!

    var rides = NSArray()
    
    @IBOutlet var noridesLabel: UILabel!
    @IBOutlet weak var activityController: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.noridesLabel.isHidden = true


        if let id = UserDefaults.standard.value(forKey: "userid")
        {
            print("Userid",id)
            
            Userid = "\(id)" as NSString
            
            self.getYourRides()

        }
        // Do any additional setup after loading the view.
        self.title = "My Rides"
        ridesTableView.dataSource = self
        ridesTableView.delegate = self

        ridesTableView.register(UINib(nibName: "RidesTableViewCell", bundle: nil), forCellReuseIdentifier: "rides")
    }
    @IBOutlet weak var ridesTableView: UITableView!

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func getYourRides(){
        
        self.activityController.startAnimating()
        let headers = [
            "cache-control": "no-cache",
            "postman-token": "238a405c-eaf6-a2b2-9b01-4306a824ca14"
        ]
        
        let urlstr = "http://139.59.71.224:3010/api/Trips?filter[where][customerId]=\(Userid!)"
        
      //  print("urlstr",urlstr)
        
        let request = NSMutableURLRequest(url: NSURL(string: urlstr)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error)
                DispatchQueue.main.async {
                }
            } else {
                let httpResponse = response as? HTTPURLResponse
                print(httpResponse!)
                do{
                    let result = try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSArray

                    print("resulr",result)
                    
                    self.rides = result
                    
                    if result.count == 0
                   {
                    DispatchQueue.main.async {

                        self.activityController.stopAnimating()

                    self.ridesTableView.isHidden = true
                    
                    self.activityController.isHidden = true
                    
                    self.noridesLabel.isHidden = false
                    }
                    }
                    
                    DispatchQueue.main.async {

                    self.ridesTableView.reloadData()
                        
                        self.activityController.stopAnimating()

                    }
                }
                
            }
        })
        
        dataTask.resume()
    }


    @IBAction func onBackTapped(_ sender: Any) {
    self.dismiss(animated: true, completion: nil)
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return rides.count;
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        

        let cell = tableView.dequeueReusableCell(withIdentifier: "rides") as! RidesTableViewCell

        cell.startView.layer.cornerRadius = cell.startView.frame.size.width/2
        cell.endView.layer.cornerRadius = cell.endView.frame.size.width/2
//        cell.startView.layer.cornerRadius = 10
//        cell.endView.layer.cornerRadius = 10
        
        let startAddress = ((rides.object(at: indexPath.row) as! NSDictionary).value(forKey: "pickAddress") as! NSDictionary).value(forKey: "city") as! NSString
        
         let endAddress = ((rides.object(at: indexPath.row) as! NSDictionary).value(forKey: "dropAddress") as! NSDictionary).value(forKey: "street") as! NSString
        
        print("startAddress",startAddress,endAddress)
        
        let startTime = (rides.object(at: indexPath.row) as! NSDictionary).value(forKey: "startTime") as! NSString
        
        let endTime = (rides.object(at: indexPath.row) as! NSDictionary).value(forKey: "endTime") as! NSString
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let dateObj = dateFormatter.date(from: startTime as String)
        dateFormatter.dateFormat = "hh:mm a"
        let startTimerFormate = dateFormatter.string(from: dateObj!)
        
        
        let endDateFormatter = DateFormatter()
        endDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let endDateObj = endDateFormatter.date(from: endTime as String)
        endDateFormatter.dateFormat = "hh:mm a"
        let endTimeFormate = endDateFormatter.string(from: endDateObj!)
        

        let totalFare = (rides.object(at: indexPath.row) as! NSDictionary).value(forKey: "totalFare") as! NSNumber
        
        let status = (rides.object(at: indexPath.row) as! NSDictionary).value(forKey: "status") as! NSString
        
        print("status",status)
        cell.startLoc.text = startAddress as String
        cell.endLocation.text = endAddress as String
        cell.startTime.text = startTimerFormate
        cell.endTime.text = endTimeFormate
        cell.fareLabel.text = "\(totalFare)"
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "YourRidesDetailsViewController") as! YourRidesDetailsViewController

//        var driverimageUrlStr : NSString!
//        var driverNameStr : NSString!
//        var cancelStatus : NSString!
//
//        var cabimageStr : NSString!
//        var cabName : NSString!
//
//        var priceStr : NSString!
//
//        var originStr : NSString!
//        var destinationStr : NSString!
        
        vc.driverNameStr = "Janardhan" //(rides.object(at: indexPath.row) as! NSDictionary).value(forKey: "startTime") as! NSString
        
        vc.Status = (rides.object(at: indexPath.row) as! NSDictionary).value(forKey: "status") as! NSString
        
        vc.cabName = "TAC GO" //(rides.object(at: indexPath.row) as! NSDictionary).value(forKey: "status") as! NSString
        
        vc.priceStr = (rides.object(at: indexPath.row) as! NSDictionary).value(forKey: "totalFare") as! NSNumber
        
        vc.originStr = ((rides.object(at: indexPath.row) as! NSDictionary).value(forKey: "pickAddress") as! NSDictionary).value(forKey: "city") as! NSString
        
        vc.destinationStr = ((rides.object(at: indexPath.row) as! NSDictionary).value(forKey: "dropAddress") as! NSDictionary).value(forKey: "street") as! NSString
        
        vc.pickuplatStr = ((rides.object(at: indexPath.row) as! NSDictionary).value(forKey: "pickAddress") as! NSDictionary).value(forKey: "latitude") as! NSNumber
        
          vc.pickuplngstr = ((rides.object(at: indexPath.row) as! NSDictionary).value(forKey: "pickAddress") as! NSDictionary).value(forKey: "longitude") as! NSNumber
        
        vc.droplatStr = ((rides.object(at: indexPath.row) as! NSDictionary).value(forKey: "dropAddress") as! NSDictionary).value(forKey: "latitude") as! NSNumber
        
        vc.droplngStr = ((rides.object(at: indexPath.row) as! NSDictionary).value(forKey: "dropAddress") as! NSDictionary).value(forKey: "longitude") as! NSNumber
        
        print("droplatStr",vc.droplatStr,vc.droplngStr)

        self.present(vc, animated: true, completion: nil)
    }
   

}
