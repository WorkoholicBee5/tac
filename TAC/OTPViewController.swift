//
//  OTPViewController.swift
//  TAC
//
//  Created by prk on 29/10/17.
//  Copyright © 2017 Diaprix. All rights reserved.
//

import UIKit
import CDAlertView

class OTPViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var otpTF: UITextField!

    @IBOutlet weak var activittController: UIActivityIndicatorView!
    var shouldRegister :Bool!
    var otpDict:NSDictionary!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        shouldRegister = false
    
        otpTF.addTarget(self, action: #selector(self.onValueChanged), for: UIControlEvents.allEditingEvents)
        
        let otp = (otpDict.object(forKey: "data") as! NSDictionary).object(forKey: "otp")!
        print(otp)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onLogout), name: NSNotification.Name(rawValue: "logout"), object: nil)
        
    }
    
    func onLogout()  {
        
        UserDefaults.standard.set(nil, forKey: "mobile")

        print("mobile Number",UserDefaults.value(forKey: "mobile")!)
        self.dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func onValueChanged() {
        if (self.otpTF.text?.characters.count)! > 4 {
            
            let otp = (otpDict.object(forKey: "data") as! NSDictionary).object(forKey: "otp")!
            print(UserDefaults.standard.value(forKey: "mobile")!)
            if self.otpTF.text! == String(describing: otp) {
                let num = UserDefaults.standard.value(forKey: "mobile") as! Int
                self.checkCustomerAvailability(mobile: Double(num))
            }else{
                
                self.otpTF.text = ""
              //  self.displayAlertWithMessage(message: "Please enter correct OTP", title: "Wrong OTP")
                
                self.displayAlertWithMessage(message: "Please enter correct OTP", title: "Wrong OTP", imagename: "alert", buttonName: "OK")
            }
            
            
            
            //            if shouldRegister {
            //
            //                let vc = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewControllerID")
            //                self.present(vc!, animated: true, completion: nil)
            //            }else{
            //                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SWRevealViewControllerID")
            //                self.present(vc!, animated: true, completion: nil)
            //            }
            
        }

    }
//    -(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
//    {
//    if (!url) {
//    UIApplication * yourapplication =[UIApplication sharedApplication];
//    NSString *outputpath =@"appname://data/";
//    NSURL *url =[NSURL URLWithString:outputpath];
//    [yourapplication openURL:url];
//    return NO;
//    }
//    
//    NSUserDefaults *defaultString =[NSUserDefaults standardUserDefaults];
//    NSString * commonString =[url absoluteString];
//    if (commonString.length<=15) {
//    //
//    }
//    else
//    {
//    [defaultString setObject:commonString forKey:@"urlString"];
//    }
//    //send info to the screen you need and can navigate
//    return YES;
//    }

    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

//        textField.text = newStr
        
//         NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
//                let newStr =
        if (textField.text?.characters.count)! >= 4 {
            
            let otp = (otpDict.object(forKey: "data") as! NSDictionary).object(forKey: "otp")!
            print(UserDefaults.standard.value(forKey: "mobile")!)
            if textField.text == otp as? String {
            self.checkCustomerAvailability(mobile: UserDefaults.standard.value(forKey: "mobile") as! Double)
            }else{
                
                textField.text = ""
               // self.displayAlertWithMessage(message: "Please enter correct OTP", title: "Wrong OTP")
                self.displayAlertWithMessage(message: "Please enter correct OTP", title: "Wrong OTP", imagename: "alert", buttonName: "OK")
            }
            
            
            
            if shouldRegister {
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewControllerID")
                self.present(vc!, animated: true, completion: nil)
            }else{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "SWRevealViewControllerID")
                self.present(vc!, animated: true, completion: nil)
            }
            
        }
        return true
    }
    
    
    @IBAction func onChangeNumberTapped(_ sender: Any) {
        
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SWRevealViewControllerID")
//
//        self.navigationController?.pushViewController(vc!, animated: true)
        
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func onResendSmsTapped(_ sender: Any) {
        
        self.activittController.startAnimating()
        let headers = [
            "content-type": "application/json",
            "cache-control": "no-cache",
            "postman-token": "b572d9c6-9183-785c-73c9-4450fad35a5d"
        ]
        let parameters = ["details": ["mobileNo": UserDefaults.standard.value(forKey: "mobile")]] as [String : Any]
        
        
        var postData:Data!
        do {
            postData =  try JSONSerialization.data(withJSONObject: parameters, options: [])
        }catch {
            
        }
        
        let request = NSMutableURLRequest(url: NSURL(string: "http://139.59.71.224:3010/api/Customers/sendOtpToCustomer")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                DispatchQueue.main.async {
                    self.activittController.stopAnimating()
                    // self.displayAlertWithMessage(message: "unable to send OTP", title: "")
                    self.displayAlertWithMessage(message: "unable to send OTP", title: "Error", imagename: "error", buttonName: "OK")
                }
            } else {
                
                do{
                    let result = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as! NSDictionary
                    DispatchQueue.main.async {
                       // UserDefaults.standard.setValue(mobile, forKey: "mobile")
                       // UserDefaults.standard.synchronize()
                        
                        print("Loginresult",result)
                        
                        self.activittController.stopAnimating()
//                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "OTPViewControllerID") as! OTPViewController
//                        vc.otpDict = result
//                        self.present(vc, animated: true, completion: nil)
                    }
                    
                    
                }catch{
                    DispatchQueue.main.async {
                        self.activittController.stopAnimating()
                        //  self.displayAlertWithMessage(message: "unable to send OTP", title: "")
                        
                        self.displayAlertWithMessage(message: "unable to send OTP", title: "OTP", imagename: "error", buttonName: "OK")
                    }
                }
                
                
            }
        })
        
        dataTask.resume()
    }
    
    func checkCustomerAvailability(mobile:Double)  {
        
        self.activittController.startAnimating()
        let headers = [
            "cache-control": "no-cache",
            "postman-token": "238a405c-eaf6-a2b2-9b01-4306a824ca14"
        ]
        
        let request = NSMutableURLRequest(url: NSURL(string: "http://139.59.71.224:3010/api/Customers?filter%5Bwhere%5D%5Band%5D%5B0%5D%5Bmobile%5D=\(mobile)")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error)
                DispatchQueue.main.async {
                    self.activittController.stopAnimating()
                }
            } else {
                let httpResponse = response as? HTTPURLResponse
                print(httpResponse)
                do{
                    var result = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as! NSArray
                    print("resulr",result)
                    if result.count == 0{
                        //need to register
                        DispatchQueue.main.async {
                            self.activittController.stopAnimating()
                        
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewControllerID")
                        self.present(vc!, animated: true, completion: nil)
                            }
                    }else{
                        DispatchQueue.main.async {
                            self.activittController.stopAnimating()
                            
                            UserDefaults.standard.setValue((result.object(at: 0) as! NSDictionary).object(forKey: "id")!, forKey: "userid")
                            UserDefaults.standard.setValue((result.object(at: 0) as! NSDictionary).object(forKey: "name")!, forKey: "name")

                            UserDefaults.standard.synchronize()
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SWRevealViewControllerID")
                        self.present(vc!, animated: true, completion: nil)
                        }
                    }
                    
                }catch{
                    
                }
            }
        })
        
        dataTask.resume()
    }
    func displayAlertWithMessage(message:String, title:String,imagename:String,buttonName:String) {
        
        
        let alert = CDAlertView(title: title, message: message, type: .custom(image: UIImage(named:imagename)!))
        
        let doneAction = CDAlertViewAction(title: buttonName)
        alert.add(action: doneAction)
        
        alert.show()

    }

    

}
