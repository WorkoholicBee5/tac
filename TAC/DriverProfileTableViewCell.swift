//
//  DriverProfileTableViewCell.swift
//  TAC
//
//  Created by Kshiteej deshpande on 07/12/17.
//  Copyright © 2017 Verizon. All rights reserved.
//

import UIKit

class DriverProfileTableViewCell: UITableViewCell {
    @IBOutlet var driverImageView: UIImageView!
    @IBOutlet var driverNameLabel: UILabel!
    @IBOutlet var statusImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
