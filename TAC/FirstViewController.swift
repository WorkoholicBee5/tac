//
//  LeftSideViewController.swift
//  TAC
//
//  Created by prk on 14/10/17.
//  Copyright © 2017 Diaprix. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Alamofire
import CDAlertView
import NVActivityIndicatorView

class FirstViewController: UIViewController,CLLocationManagerDelegate,GMSMapViewDelegate,UITextFieldDelegate,UICollectionViewDelegate,UICollectionViewDataSource{
  
    
    @IBOutlet weak var rideLaterDatePicker: UIDatePicker!
   
    var cirlce: GMSCircle!
    
    var obj : objects!

    var lat : NSNumber!
    var lng : NSNumber!
    var descriptiontxt: String!
    var statustxt : String!
    
    var state : String!
    
    var polyline = GMSPolyline()
    

    var driverName : NSString!
    
    
    var distancevalue : Int = 0
    var durationvalue : Int = 0
    
    var datePicker : UIDatePicker!
    var myView : UIView!
    var cancelmyview : String!
    
    
    var category : NSString! = "TAC GO"
    
    var categoryimage : String!
    
    var timer = Timer()
    
    var durationArr = NSMutableArray()
    var duration : NSString!

    
    var pickupStreet :NSString! = "street"
    
    var pickupState :NSString! = "state"
    var pickupCity :NSString! = "city"
    var pickupZipCode :NSString! = "zipcode"
    
    var SelectedDate :NSString! = "date"
    
    var m = GMSMarker()
    
    var mobileNo : NSString!
    var Userid : NSString!
    var username : NSString!
    
    
    var dropStreet :NSString! = "street"
    var dropCity :NSString! = "city"
    var dropState :NSString! = "state"
    var dropZipCode :NSString! = "zipcode"
    
    var currentlat : NSNumber!
    var currentlng : NSNumber!
    
    
    
    var getTotalFareStatus : NSString!
    
    
   // var urlStrig : NSString!
    var GetTotalView : UIView!
    
    
    var nvactivityIndicatorView = NVActivityIndicatorView(frame: CGRect(x:8, y:3,width:80, height:30), type: .ballBeat, color: UIColor.green, padding: nil)
    // MARK: Outlets
    
    @IBOutlet weak var label: UILabel!
    
    @IBOutlet weak var enterDropLocationButton: UIButton!
    
    @IBOutlet weak var cashView: UIView!
    var removePath:GMSPolyline!
//    var polylineArray:NSMutableArray!
    @IBOutlet weak var pickUpBtn: UIButton!
    var routes:NSArray!
    var  mapView = GMSMapView()
    @IBOutlet weak var fromView: UIView!
    @IBOutlet weak var startLocationLabel: UILabel!
    @IBOutlet weak var cabsView: UIView!
    @IBOutlet weak var toView: UIView!
    @IBOutlet weak var confirmBookingView: UIView!
    @IBOutlet var carimageview: UIImageView!
    @IBOutlet weak var carNameLabel: UILabel!
    @IBOutlet weak var carTimeLabel: UILabel!
    @IBOutlet weak var carDistanceLabel: UILabel!
    
    @IBOutlet weak var fareLabel: UILabel!
    @IBOutlet weak var fareView: UIView!
    @IBOutlet weak var centerImageView: UIImageView!
    @IBOutlet var fareActivityIndicatorView: UIActivityIndicatorView!
    
    @IBOutlet var homeactivityIndicatorView: UIActivityIndicatorView!
    
    
    var locationManager = CLLocationManager()
//    var gPlaces = GMSPlace()
    var dummyView = UIView()
    var validDropLocation = false
    var didFindMyLocation = false
    var carsView:UIView!
    var statestring = "pick"
    var ridenowtappedstatus = "no"
    var distance = CGFloat()
    var time = CGFloat()
    var mainarray : NSArray!
    
    var fromlocation = CLLocationCoordinate2D()
    var tolocation  = CLLocationCoordinate2D()
    
    
    @IBOutlet weak var rideLaterButton: UIButton!
    @IBOutlet weak var rideNowButton: UIButton!
    //@IBOutlet weak var : UIBarButtonItem!
    @IBOutlet var menuButton: UIButton!
    @IBOutlet var backButton: UIButton!
    
    @IBOutlet var navigationbarView: UIView!
    
    @IBOutlet var driverview: UIView!
    
    @IBOutlet var carImageview: UIImageView!
    @IBOutlet var carnostatusLabel: UILabel!
    @IBOutlet var carNoLabel: UILabel!
    @IBOutlet var driverImageView: UIImageView!
    
    @IBOutlet var driverNameLabel: UILabel!
    
    @IBOutlet var vehicleNameLabel: UILabel!
    
    
    var tag = 0
    
    var shouldDisplaySearchPlaces = false
    
    
    func ontextFieldTapped()  {
        
        shouldDisplaySearchPlaces = true
    }
    
    func tappedOnView()  {
        
    }
 
override func viewDidLoad() {
        super.viewDidLoad()
    
    self.durationArr = ["duration","duration","duration"]

        self.getcategeory()
    
    UserDefaults.standard.setValue(nil, forKey: "lat")
    UserDefaults.standard.setValue(nil, forKey: "lng")
    UserDefaults.standard.setValue(nil, forKey: "descriptiontxt")
    
    
    self.view.addSubview(navigationbarView)
    self.backButton.isHidden = true
    self.menuButton.isHidden = false
   // let userDefaults = UserDefaults.standard
  //  let value  = userDefaults.string(forKey: "mobile")
  //  print(value!)
    
    //UILabel(frame: CGRectMake(0, 0, image.size.width, image.size.height))
   
    if let mNumber = UserDefaults.standard.value(forKey: "mobile"){
        
        print("mobile",mNumber)
        
        mobileNo = "\(mNumber)" as NSString
    }
    if let id = UserDefaults.standard.value(forKey: "userid")
    {
        print("Userid",id)
        
        Userid = "\(id)" as NSString
    }
    if let name = UserDefaults.standard.value(forKey: "name")
    {
        print("username",name)
        
        username = name as! NSString
    }
    
    if UserDefaults.standard.value(forKey: "token") != nil
    {
        postdeviceToken()
    }
    
        let tapGetureView = UITapGestureRecognizer(target: self, action: #selector(self.tappedOnView))
        self.mapView.addGestureRecognizer(tapGetureView)
        
        categeoryCollectionView.delegate = self
        categeoryCollectionView.dataSource = self
        self.confirmBookingView.isHidden = true
        pickUpTF.delegate = self
        dropTF.delegate = self
//        statestring = "none"
        self.getCabsData()
        toView.isHidden = true
        self.enterDropLocationButton.isHidden = false
       // self.getnearbyvehicles()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.ontextFieldTapped))
        tapGesture.numberOfTapsRequired = 2
        self.pickUpTF.addGestureRecognizer(tapGesture)
        self.dropTF.addGestureRecognizer(tapGesture)
        
        // Do any additional setup after loading the view.
        
        //swreveal Viewcontroller
        if self.revealViewController() != nil {
          //  menuButton.target = self.revealViewController()
          //  menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            menuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            //menuButton.setBackgroundImage(UIImage.init(named: "marker_ball"), for:.normal, barMetrics: UIBarMetrics)
            
        }
        
        
     //   self.gmscontroller.delegate = self
     //   self.gmscontroller1.delegate = self
        
        
//         location manager
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 0
        locationManager.startUpdatingLocation()
        
        
        //map view
        
        mapView  = GMSMapView(frame: CGRect(x: 0, y: 80, width: self.view.frame.size.width, height: self.view.frame.size.height-40))
        let mapInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 180.0, right: 0.0)
        mapView.padding = mapInsets
       
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        mapView.delegate = self
        self.view.addSubview(mapView)
    
    self.view.addSubview(self.driverview)

    
     //    setting camera position
    let locManager = CLLocationManager()
    locManager.requestWhenInUseAuthorization()
    
    var currentLocation: CLLocation!
    
    currentLocation = locManager.location
    
   // let cameraPosition = GMSCameraPosition(target: CLLocationCoordinate2D(latitude: 12.873225, longitude: 77.614256), zoom: 10, bearing: 3, viewingAngle: 3)

    currentlat = 12.873225
    currentlng = 77.614256
    
    if currentLocation != nil
    {
    print("currentlocation-lat",currentLocation.coordinate.latitude)
    print("currentlocation-lng",currentLocation.coordinate.longitude)
        
        currentlng = currentLocation.coordinate.latitude as NSNumber
        currentlng = currentLocation.coordinate.longitude as NSNumber
    }
    let cameraPosition = GMSCameraPosition(target: CLLocationCoordinate2D(latitude: currentlng as! CLLocationDegrees, longitude: currentlng as! CLLocationDegrees), zoom: 10, bearing: 3, viewingAngle: 3)
    mapView.camera = cameraPosition

        self.view.bringSubview(toFront: fromView)
        
        self.view.bringSubview(toFront: cabsView)
        self.view.bringSubview(toFront: rideNowButton)
        self.view.bringSubview(toFront: rideLaterButton)
        self.view.bringSubview(toFront: self.centerImageView)
        self.view.bringSubview(toFront: pickUpBtn)
    
    }
   

    @IBAction func Cancel(_ sender: Any) {
        
       // self.toolBarView.isHidden = true
       // self.Datepicker.isHidden = true
    }
    
    @IBOutlet weak var rideLaterView: UIView!
    @IBOutlet weak var pickupDateTimeLabel: UILabel!
    @IBOutlet weak var advBookingFeeLabel: UILabel!
    @IBOutlet weak var fareDetailsLabel: UILabel!
    @IBOutlet weak var TotalFareLabel: UILabel!
    

    @IBOutlet weak var pickUpTF: UITextField!
    
    @IBOutlet weak var categeoryCollectionView: UICollectionView!
    @IBOutlet weak var dropTF: UITextField!
    var centerMapCoordinate:CLLocationCoordinate2D!

   // let gmscontroller = GMSAutocompleteViewController()
   // let gmscontroller1 = GMSAutocompleteViewController()
    
    func places(status: String, lat: NSNumber,lng: NSNumber,description: String) {
        
        if status == "pickuptxt" {
            
            let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(lng), zoom: 15.0)
            self.mapView.camera = camera
            pickUpTF.text = description
        }
        if status == "droptxt" {
            
            let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(lng), zoom: 15.0)
            self.mapView.camera = camera
            dropTF.text = description
            validDropLocation = true
        }
        
    }
    
//    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
//
//        if viewController == gmscontroller {
//
//
//        let camera = GMSCameraPosition.camera(withLatitude: place.coordinate.latitude, longitude: place.coordinate.longitude, zoom: 15.0)
//        self.mapView.camera = camera
//       pickUpTF.text = place.formattedAddress
//        self.dismiss(animated: true, completion: nil)
//             }
//        if viewController == gmscontroller1 {
//
//
//            let camera = GMSCameraPosition.camera(withLatitude: place.coordinate.latitude, longitude: place.coordinate.longitude, zoom: 15.0)
//            self.mapView.camera = camera
//            dropTF.text = place.formattedAddress
//            validDropLocation = true
//
//            self.dismiss(animated: true, completion: nil)
//        }
//
//    }
//    func placeAutocomplete() {
//        let visibleRegion = mapView.projection.visibleRegion()
//        let bounds = GMSCoordinateBounds(coordinate: visibleRegion.farLeft, coordinate: visibleRegion.nearRight)
//
//        let autocompleteController = GMSAutocompleteViewController()
//        viewController.autocompleteBounds = bounds
//        autocompleteController.delegate = self
//        present(autocompleteController, animated: true, completion: nil)
//    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let array = mainarray {
            return array.count
        }
        else
        {
            return 0
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = categeoryCollectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! categoryCollectionViewCell
        
        if cell.isSelected
        {
            cell.categoryImageView.backgroundColor = UIColor.orange
        }
        else
        {
            cell.categoryImageView.backgroundColor = UIColor.clear
        }
        
        if let array = mainarray{
            
            print("mainArray",mainarray)
            cell.categeoryLabel.text = (array.object(at: indexPath.row)as! NSDictionary).object(forKey: "name") as? String
            
            cell.categoryImageView.backgroundColor = UIColor.clear

        if(indexPath.row == 0)
        {

            categeoryCollectionView.selectItem(at: indexPath, animated: true, scrollPosition: UICollectionViewScrollPosition.centeredHorizontally)
             cell.categoryImageView.backgroundColor = UIColor.orange

            category = (array.object(at: indexPath.row)as! NSDictionary).object(forKey: "name") as! NSString
            
            categoryimage = (array.object(at: indexPath.row)as! NSDictionary).value(forKey: "imageUrl") as! String
            
            print("category",category)
            print("categoryimage",categoryimage)
            
            cell.distanceMatrixLabel.text = "Duration" //(durationArr.object(at: indexPath.row) as? String)
            cell.distanceMatrixLabel.addSubview(nvactivityIndicatorView)
            
            
            if (duration != nil)
            {
            cell.distanceMatrixLabel.text = duration as String? //(durationArr.object(at: indexPath.row) as? String)
            }
        }
        else if(indexPath.row == 1)
        {
            cell.distanceMatrixLabel.text = "Duration" //(durationArr.object(at: indexPath.row) as? String)
            cell.distanceMatrixLabel.addSubview(nvactivityIndicatorView)
            }
            else if(indexPath.row == 2)
        {
            cell.distanceMatrixLabel.text = "Duration" //(durationArr.object(at: indexPath.row) as? String)
            cell.distanceMatrixLabel.addSubview(nvactivityIndicatorView)
            }
            
            let first = "http://139.59.71.224:3010/"
            print("imageurl",(array.object(at: indexPath.row)as! NSDictionary).value(forKey: "imageUrl")!)
            let second = (array.object(at: indexPath.row)as! NSDictionary).value(forKey: "imageUrl") as! String
            
            let urlstring = first + second as String

                let url : NSString = urlstring as NSString
                let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
                let searchURL : NSURL = NSURL(string: urlStr as String)!
                print(searchURL)
              //  let url = URL.init(string: "\(urlstring)")
                let data = try? Data(contentsOf: searchURL as URL)
              //  let imagedata = try? Data(contentsOf: url! as URL)

                cell.categoryImageView.image = UIImage(data: data!)
               // cell.categoryImageView.af_setImage(withURL: downloadURL as URL)
            

        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        //add here
       // let selectedIndexPath = IndexPath(item: 0, section: 0)
      //  categeoryCollectionView.selectItem(at: selectedIndexPath, animated: false, scrollPosition: .right)
    }

   func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        print("User tapped on item \(indexPath.row)")

        let selectedCell = collectionView.cellForItem(at: indexPath)! as! categoryCollectionViewCell
        
        category = (mainarray.object(at: indexPath.row)as! NSDictionary).object(forKey: "name") as! NSString
        
        categoryimage = (mainarray.object(at: indexPath.row)as! NSDictionary).value(forKey: "imageUrl") as! String
        
        print("category",category)
        
        print("cell no",indexPath.row)
        
            selectedCell.categoryImageView.backgroundColor = UIColor.orange
        
        self.getnearbyvehicles()
     
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath)

    {
       // print("User tapped on item \(indexPath.row)")

        let cellToDeSelect = collectionView.cellForItem(at: indexPath) as! categoryCollectionViewCell
        
       //  categeoryCollectionView.deselectItem(at: indexPath, animated: false)
        
            cellToDeSelect.categoryImageView.backgroundColor = UIColor.clear
    }
   
    func selcetCar(sender:UIButton)  {
        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("error auto complete\(error)")
    }
    
    @IBAction func onCashTapped(_ sender: Any) {
        
        self.cashView.isHidden = false
        self.view.bringSubview(toFront: self.cashView)
    }
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
         self.dismiss(animated: true, completion: nil)
    }
    
    // text field delegates
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
     
     return true
    }
    public func textFieldDidBeginEditing(_ textField: UITextField)
    {
      
        self.pickUpTF.resignFirstResponder()
        self.dropTF.resignFirstResponder()

      
            if textField == self.pickUpTF
        {
           
        
         //   let gmscontroller = GMSAutocompleteViewController()
            
                if self.statestring == "drop"{
                    self.statestring = "pick"
                   
                    let camera = GMSCameraPosition.camera(withLatitude: (fromlocation.latitude), longitude: (fromlocation.longitude), zoom: 15.0)
                    mapView.animate(to: camera)
                    
                    print("fromlocation.latitude",fromlocation.latitude)
                    print("fromlocation.longitude",fromlocation.longitude)

                    self.centerImageView.image = UIImage(named: "marker_ball-1")
                    
                }else{
                   // self.present(self.gmscontroller, animated: true, completion: nil)
                    
                    self.statustxt = "pickuptxt"
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchPlacesViewController")
                    self.present(vc!, animated: true, completion: nil)
            }
                
        
        }
            if textField == self.dropTF {
                
                if self.statestring == "pick"{
                    self.statestring = "drop"
                    
                    let camera = GMSCameraPosition.camera(withLatitude: (tolocation.latitude), longitude: (tolocation.longitude), zoom: 15.0)
                    mapView.animate(to: camera)
                    
                    
                    self.centerImageView.image = UIImage(named: "marker_ball")
                   // self.view.bringSubview(toFront: centerImageView)
                    
                }else{
                    //self.present(self.gmscontroller1, animated: true, completion: nil)
                    
                    self.statustxt = "droptxt"
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchPlacesViewController")
                    self.present(vc!, animated: true, completion: nil)
                }

                
//            self.gmscontroller1.delegate = self
//                    if shouldDisplaySearchPlaces{
//
//                    self.present(self.gmscontroller1, animated: true, completion: nil)
//
////                    }
//             self.centerImageView.image = UIImage(named: "marker_ball")
//                    let camera = GMSCameraPosition.camera(withLatitude: (tolocation.latitude), longitude: (tolocation.longitude), zoom: 15.0)
//                    mapView.animate(to: camera)
                
        
        }
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        

        //drawpath(startlocation: fromlocation, endlocation: tolocation)
    }
   
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {

        return true
        
    }
    
    @IBAction func enterDropLocationButtonTapped(_ sender: Any) {
        
      //  self.present(self.gmscontroller1, animated: true, completion: nil)
        
        self.statustxt = "droptxt"
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchPlacesViewController")
        self.present(vc!, animated: true, completion: nil)
        
    }
    
//    func drawpath()
//    {
//         self.drawpath(startlocation: fromlocation, endlocation: tolocation)
//    }

    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        pickUpTF.resignFirstResponder()
        dropTF.resignFirstResponder()
        
        return true
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !(UserDefaults.standard.value(forKey: "isFirstLogin") != nil) {
            UserDefaults.standard.set(true, forKey: "isFirstLogin")
            UserDefaults.standard.synchronize()
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "CabTourViewController")
            self.present(vc!, animated: true, completion: nil)
        }
    
        mapView.clear()

      //  if UserDefaults.standard.value(forKey: "token") != nil
        if UserDefaults.standard.value(forKey: "descriptiontxt") != nil {
            
            self.lat = UserDefaults.standard.value(forKey: "lat") as! NSNumber
            self.lng = UserDefaults.standard.value(forKey: "lng") as! NSNumber
            self.descriptiontxt = UserDefaults.standard.value(forKey: "descriptiontxt") as! String
            
            print("places",statustxt,lat,lng,descriptiontxt)
            places(status: statustxt, lat: lat, lng: lng, description: descriptiontxt)
            
            centerImageView.isHidden = false
            
            if statustxt == "pickuptxt"
            {
                
                fromlocation.latitude = CLLocationDegrees(UserDefaults.standard.value(forKey: "lat") as! NSNumber)
                fromlocation.longitude = CLLocationDegrees(UserDefaults.standard.value(forKey: "lng") as! NSNumber)
                
                print("fromlocation",fromlocation.latitude,fromlocation.longitude)
                
                let camera = GMSCameraPosition.camera(withLatitude: (fromlocation.latitude), longitude: (fromlocation.longitude), zoom: 15.0)
                mapView.animate(to: camera)

            self.centerImageView.image = UIImage(named: "marker_ball-1")
                
            }
            if statustxt == "droptxt"
            {

                tolocation.latitude = CLLocationDegrees(UserDefaults.standard.value(forKey: "lat") as! NSNumber)
                tolocation.longitude = CLLocationDegrees(UserDefaults.standard.value(forKey: "lng") as! NSNumber)
                
                print("tolocation",tolocation.latitude,tolocation.longitude)
                
                let camera = GMSCameraPosition.camera(withLatitude: (tolocation.latitude), longitude: (tolocation.longitude), zoom: 15.0)
                mapView.animate(to: camera)

                self.centerImageView.image = UIImage(named: "marker_ball")
            }
            
           // self.drawpath(startlocation: fromlocation, endlocation: tolocation)
        }
        
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D){
        
//        print(coordinate.latitude)
//        print(coordinate.longitude)
//        
//        let mar = GMSMarker()
//        mar.title = "tapped"
//        mar.position = coordinate
//        mar.map = mapView
//        
//        let cameraPosition = GMSCameraPosition(target: coordinate, zoom: 15, bearing: 2, viewingAngle: 2)
//        mapView.camera = cameraPosition
//        
//        let add = GMSPlacesClient()


        self.cashView.isHidden = true
        
        self.rideLaterView.isHidden = true
       // self.myView.isHidden = true

        self.fareView.isHidden = true
        
    }
    
//    - (void)wasCancelled:(GMSAutocompleteViewController *)viewController;
    
    

    @IBAction func setPickUpTappes(_ sender: Any) {
        dropTF.becomeFirstResponder()
        
        self.view.sendSubview(toBack: pickUpBtn)
        
        self.view.bringSubview(toFront: toView)
        
    }
    @IBAction func onRideNowTapped(_ sender: Any) {
        
        mapView.clear()
        
        
        if (self.dropTF.text?.isEmpty)!
        {
            self.statestring = "drop"
            
            //    self.present(self.gmscontroller1, animated: true, completion: nil)
            
            self.statustxt = "droptxt"
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchPlacesViewController")
            self.present(vc!, animated: true, completion: nil)
            
        }
        else
        {
            
            self.view.sendSubview(toBack: pickUpBtn)
            
            self.view.bringSubview(toFront: toView)
            
          //  marker.icon = UIImage(named: "marker_ball-1")!
           // marker.map = self.mapView
           // marker.position = fromlocation
            centerImageView.isHidden = true
            self.view.sendSubview(toBack: self.centerImageView)
            
           // marker1.icon = UIImage(named: "marker_ball")!
           // marker1.position = tolocation
           // marker1.map = self.mapView
            ridenowtappedstatus = "yes"
            
            if validDropLocation {
                
                
                //                   marker1.icon = UIImage(named: "marker_ball")!
                //                   marker1.position = tolocation
                //                   marker1.map = self.mapView
                ridenowtappedstatus = "yes"
                
                
            }
            
            if !validDropLocation {
                self.enterDropLocationButton.isHidden = false
            }else{
                self.enterDropLocationButton.isHidden = true
            }
            self.confirmBookingView.isHidden = false
            self.view.bringSubview(toFront: confirmBookingView)
            
            // change navigation button to back button
            
            menuButton.isHidden = true
            backButton.isHidden = false
            
            
            marker.icon = UIImage(named: "marker_ball-1")!
            marker.map = self.mapView
            marker.position = fromlocation
            
            marker1.icon = UIImage(named: "marker_ball")!
            marker1.map = self.mapView
            marker1.position = tolocation
            
            self.drawpath(startlocation: fromlocation, endlocation: tolocation)

            backButton.addTarget(self, action: #selector(self.backButtonTapped), for: .touchUpInside)
            //        let backButton = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(self.backButtonTapped))
            //        self.navigationItem.setLeftBarButton(backButton, animated: true)
            
            // self.getDistance()
        }
    }
    @IBAction func onRideLater(_ sender: Any) {
       
        mapView.clear()

        if (self.dropTF.text?.isEmpty)!
        {
            //self.displayAlertWithMessage(message: "Please Enter Drop Point", title: "Empty")
       
            self.statestring = "drop"
            
            //self.present(self.gmscontroller1, animated: true, completion: nil)
            
            self.statustxt = "droptxt"
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchPlacesViewController")
            self.present(vc!, animated: true, completion: nil)
        }
        else
        {
            self.view.sendSubview(toBack: pickUpBtn)
            
            self.view.bringSubview(toFront: toView)
            
           // marker.icon = UIImage(named: "marker_ball-1")!
          //  marker.map = self.mapView
          //  marker.position = fromlocation
            centerImageView.isHidden = true
            self.view.sendSubview(toBack: self.centerImageView)

          //  marker1.icon = UIImage(named: "marker_ball")!
          //  marker1.position = tolocation
          //  marker1.map = self.mapView
            ridenowtappedstatus = "yes"

            self.getDistance(origin:self.pickUpTF.text! as NSString, destination: self.dropTF.text! as NSString)
            
            self.enterDropLocationButton.isHidden = true
            
            self.confirmBookingView.isHidden = false
            
            // let screenSize: CGRect = UIScreen.main.bounds
            myView = UIView(frame: CGRect(x: 0, y: 400, width: self.view.frame.size.width, height: 300))
            self.view.addSubview(myView)
            
            // DatePicker
            self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 220))
            self.datePicker.backgroundColor = UIColor.white
            self.datePicker.datePickerMode = UIDatePickerMode.dateAndTime
            // textField.inputView = self.datePicker
            
            self.myView.addSubview(datePicker)
            
            // ToolBar
            let toolBar = UIToolbar()
            toolBar.barStyle = .default
            toolBar.isTranslucent = true
            toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
            toolBar.sizeToFit()
            
            // Adding Button ToolBar
            let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(FirstViewController.doneClick))
            let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(FirstViewController.cancelClick))
            toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
            
            toolBar.isUserInteractionEnabled = true
            
            self.myView.addSubview(toolBar)
            
            menuButton.isHidden = true
            backButton.isHidden = false
            
            self.cancelmyview = "closeMyview"
            
            marker.icon = UIImage(named: "marker_ball-1")!
            marker.map = self.mapView
            marker.position = fromlocation
            
            marker1.icon = UIImage(named: "marker_ball")!
            marker1.map = self.mapView
            marker1.position = tolocation
            
            drawpath(startlocation: fromlocation, endlocation: tolocation)
            
            backButton.addTarget(self, action: #selector(self.backButtonTapped), for: .touchUpInside)
        }
        
    }
    @IBAction func RideLaterconformBookingButton(_ sender: Any) {
        
        self.circleAnimation()
        
        self.rideLaterView.isHidden = true
        
        let dateFormatter: DateFormatter = DateFormatter()

        dateFormatter.dateFormat = "yyyy"
        let year = dateFormatter.string(from: datePicker.date)
        print("year",year)
        
        dateFormatter.dateFormat = "MM"
        let month = dateFormatter.string(from: datePicker.date)
        print("month",month)
        
        dateFormatter.dateFormat = "dd"
        let day = dateFormatter.string(from: datePicker.date)
        print("day",day)
        
        dateFormatter.dateFormat = "hh"
        let hour = dateFormatter.string(from: datePicker.date)
        print("hour",hour)
        
        
        dateFormatter.dateFormat = "mm"
        let minute = dateFormatter.string(from: datePicker.date)
        print("minute",minute)
        
        sendRideLaterNotificationToDriver(year: Int(year)!, month: Int(month)!, day: Int(day)!, hour: Int(hour)!, minute: Int(minute)!)
    }
    
    func doneClick() {
        

        let dateFormatter: DateFormatter = DateFormatter()

        // Set date format
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"

        // Apply date format
        let selectedDate: String = dateFormatter.string(from: datePicker.date)

        print("Selected value \(selectedDate)")

        
        self.myView.isHidden = true
        
        self.pickupDateTimeLabel.text = "pickup Date and Time:" + selectedDate
        
        self.getRidLaterTotalFare()
        
        dateFormatter.dateFormat = "MM-dd-yyyy hh:mm a"
        self.SelectedDate = dateFormatter.string(from: datePicker.date) as NSString
        
        marker.icon = UIImage(named: "marker_ball-1")!
        marker.map = self.mapView
        marker.position = fromlocation
        
        marker1.icon = UIImage(named: "marker_ball")!
        marker1.map = self.mapView
        marker1.position = tolocation
        
        drawpath(startlocation: fromlocation, endlocation: tolocation)
        
    }
    func cancelClick() {

        self.myView.isHidden = true
        
        self.m.map = nil
        
    }
    
    func getRidLaterTotalFare() {
        
        let headers = [
            "cache-control": "no-cache",
            "postman-token": "f7de3b7c-efa6-8adf-7042-45616a78be31"
        ]
      
        // category = "TAC GO"
        
        let urlString = "http://139.59.71.224:3010/api/Categories/totalFare?details=%7B%20%22category%22%3A%22\(category!)%22%2C%20%22distance%22%3A%20\(distancevalue)%2C%20%22duration%22%3A%20\(durationvalue)%2C%20%22waitingTime%22%3A%200%20%7D"
        
        print("getRidLaterTotalFaredistance",distancevalue)
        print("getRidLaterTotalFareduration",durationvalue)
        
        let url = urlString.replacingOccurrences(of: " ", with: "%20")
        
        print(urlString)
        let request = NSMutableURLRequest(url: NSURL(string: url)! as URL,cachePolicy: .useProtocolCachePolicy,timeoutInterval: 10.0)
        
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error as Any)
            } else {
                let httpResponse = response as? HTTPURLResponse
                print(httpResponse as Any)
                do{
                    let  json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as! NSDictionary
                    print("getRidLaterTotalFare",json)
                    
                    DispatchQueue.main.async {
                        //
                        
                        let dataDic = json.value(forKey: "data") as! NSDictionary
                        
                        print("data",dataDic)
        
                        if (dataDic.value(forKeyPath: "isSuccess")as! Int == 1)
                        {
                        self.advBookingFeeLabel.text = "Advance Booking Fee :" + "20" + " Rs"
                      
                        self.fareDetailsLabel.text = "Approximate Fare Details :" + String(dataDic.value(forKeyPath: "totalFareWithTax") as! Double) + " Rs"
                        
                        print("fareDetailsLabel",self.fareDetailsLabel.text!)
                        
                        let i = dataDic.value(forKeyPath: "totalFareWithTax") as! Double
                        
                        let total = i+20
                        print("total",total)

                        self.TotalFareLabel.text = "Total Fare    :" + String(total) + "Rs"
                        
                        self.rideLaterView.isHidden = false
                        self.view.bringSubview(toFront: self.rideLaterView)
                        
                    }
                        else
                        {
                           // let errorMessage = dataDic.value(forKeyPath: "message")
                            
                          //  self.displayAlertWithMessage(message: errorMessage as! String, title: "No Cabs")
                            self.displayAlertWithMessage(message: "error", title: "Error", imagename: "error", buttonName: "OK")
                        }
                    
                }
                }
                catch{
                    
                }
            }
        })
        
        dataTask.resume()
    }
    
    func getDistance(origin:NSString,destination:NSString) {
        
       // let origin = self.pickUpTF.text
       // let destination = self.dropTF.text
        
        print("origin",origin)
        print("destination",destination)
        
    let urlstring = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=\(String(describing: origin))&destinations=\(String(describing: destination)),&key=AIzaSyDvnSkHURaHrAe8RaB0S4NYxrzOCZT-qpU"
        
    //    let urlstring = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=\(String(describing: origin))&destinations=\(String(describing: destination))&mode=driving&departure_time=now&traffic_model=optimistic&key=AIzaSyDvnSkHURaHrAe8RaB0S4NYxrzOCZT-qpU"
        
        print("urlstring",urlstring)

        let newurlStr = urlstring.replacingOccurrences(of: " ", with: "%20")

        
        let request = URLRequest(url: URL(string:newurlStr)!)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {   // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {  // check for http errors
                
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                
                print("response = \(String(describing: response))")
                
            }
            
            do{
                
                let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments) as! NSDictionary
                
                print("getDistance",json)
                
                let distancematrixData = json.value(forKey: "rows") as! NSArray
                
                print("rows",distancematrixData)
                
                let stetus = (((distancematrixData.object(at: 0)as! NSDictionary).value(forKey: "elements")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "status")as! String
                
                print("stetus",stetus)
                
                if stetus == "OK"
                {
                let distance = ((((distancematrixData.object(at: 0)as! NSDictionary).value(forKey: "elements")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "distance")as! NSDictionary).object(forKey: "value")as! Int
                
                print("value",distance)

                let duration = ((((distancematrixData.object(at: 0)as! NSDictionary).value(forKey: "elements")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "duration")as! NSDictionary).object(forKey: "value")as! Int
                
                print("durationvalue",duration)
                print("distance",distance)

                    self.distancevalue = distance / 1000
                
                    self.durationvalue = duration / 60

                    print("distanceValue",self.distancevalue)
                    print("durationValue",self.durationvalue)
                    
                        self.gettingtotalfare(distance: self.distancevalue, duration: self.durationvalue)
                    
                }
                else
                {
                   self.displayAlertWithMessage(message: "Error in Distance", title: "Error", imagename: "error", buttonName: "OK")
                    
                    self.fareActivityIndicatorView.stopAnimating()
                    
                    self.fareActivityIndicatorView.isHidden = true
                    
                    self.fareView.isHidden = true
                }

            }
                
            catch let error as NSError{
                
                print(error)
                
            }
            
        }
        
        task.resume()
    }
    
    func getDistancebetweenwCabAndUser(origin:NSString,destination:NSString) {
    
    print("origin",origin)
    print("destination",destination)
        
        let urlstring = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=\(String(describing: origin))&destinations=\(String(describing: destination))&mode=driving&departure_time=now&traffic_model=optimistic&key=AIzaSyDvnSkHURaHrAe8RaB0S4NYxrzOCZT-qpU"
    
    print("urlstring",urlstring)
    
    let newurlStr = urlstring.replacingOccurrences(of: " ", with: "%20")
    
    
    let request = URLRequest(url: URL(string:newurlStr)!)
    
    let task = URLSession.shared.dataTask(with: request) { data, response, error in
    guard let data = data, error == nil else {   // check for fundamental networking error
    print("error=\(String(describing: error))")
    return
    }
    
    if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {  // check for http errors
    
    print("statusCode should be 200, but is \(httpStatus.statusCode)")
    
    print("response = \(String(describing: response))")
    
    }
    
    do{
    
    let json = try JSONSerialization.jsonObject(with: data, options:.allowFragments) as! NSDictionary
    
    print("getDistance",json)
    
    let distancematrixData = json.value(forKey: "rows") as! NSArray
    
    print("rows",distancematrixData)
    
    //  let distancevale = ((((distancematrixData.object(at: 0) as AnyObject).value(forKey: " elements")as! NSArray).object(at: 0).value(forKey: "distance")as! NSDictionary).value(forKey: "value")as! Int
    
    let stetus = (((distancematrixData.object(at: 0)as! NSDictionary).value(forKey: "elements")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "status")as! NSString
    
    print("stetus",stetus)
    
    if stetus == "OK"
    {
    let distance = ((((distancematrixData.object(at: 0)as! NSDictionary).value(forKey: "elements")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "distance")as! NSDictionary).object(forKey: "text")as! String
    
    print("value",distance)
    
    
    print("distance",distance)
        
        //self.duration = duration as NSString
        
      //  for i in 0..<self.duration.count
      //  {
            let durationstr = ((((distancematrixData.object(at: 0)as! NSDictionary).value(forKey: "elements")as! NSArray).object(at: 0)as! NSDictionary).value(forKey: "duration")as! NSDictionary).object(forKey: "text")as! String
            
       // self.durationvalue.append
        
        
        self.duration = durationstr as NSString
        
      //  self.durationArr.insert("duration", at: 0)

        if (self.duration != nil)
        {
            self.durationArr.insert(self.duration, at: 0)
            
            print("durationArr",self.durationArr)

        }
        print("durationArr",self.duration)

      //  }
        DispatchQueue.main.async {
            
            // self.progressBar.isHidden = true
            
            //self.categeoryCollectionView.reloadData()
            
            // for i in 0..<self.durationArr.count
            // {
            let cell = self.categeoryCollectionView.cellForItem(at: IndexPath.init(row: 0, section: 0)) as? categoryCollectionViewCell
            
            cell?.distanceMatrixLabel.text = self.durationArr.object(at: 0) as? String
            //  }
        }
    
    }
   // }
    else
    {
    
    }
       
    }
    
    catch let error as NSError{
    
    print(error)
    
    }
    
    }
      
    task.resume()
    }
    

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if !didFindMyLocation {
//            let changed = change?[.newKey] as? Float
//            let myLocation:CLLocation = changed[NSKeyValueChangeKey] as CLLocation
//            mapView.camera = GMSCameraPosition.camera(withTarget: myLocation.coordinate, zoom: 10)
        }else{
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func backButtonTapped()  {
        

        self.backButton.isHidden = true
        self.menuButton.isHidden = false
        
        driverview.isHidden = true
        
        self.fareView.isHidden = true
        
        self.rideLaterView.isHidden = true
        
        if cancelmyview == "closeMyview"
        {
            self.cancelmyview = ""
            self.myView.isHidden = true
        }
        //self.myView.isHidden = true
        
        self.mapView.clear()
         let camera = GMSCameraPosition.camera(withLatitude: (fromlocation.latitude), longitude: (fromlocation.longitude), zoom: 15.0)
        
        centerImageView.isHidden = false
        self.view.bringSubview(toFront: centerImageView)
        mapView.animate(to: camera)
        statestring = "pick"
        ridenowtappedstatus = "no"
        self.view.sendSubview(toBack: confirmBookingView)
       // self.navigationItem.setLeftBarButton(menuButton, animated: true)
        self.menuButton.isHidden = false
        self.backButton.isHidden = true
        self.centerImageView.image = UIImage(named: "marker_ball-1")
        
        self.m.map = nil
    }

    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus){
        
        
        if (status == .authorizedAlways || status == .authorizedWhenInUse){
            mapView.isMyLocationEnabled = true
        }else{
            mapView.isMyLocationEnabled = false
            // display alert to change location for app in settings
        }
    }
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//
//
//        let location = locations.last
//        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 15.0)
//            fromlocation.latitude = (location?.coordinate.latitude)!
//            fromlocation.longitude = (location?.coordinate.longitude)!
//            tolocation.latitude = (location?.coordinate.latitude)!
//            tolocation.longitude = (location?.coordinate.longitude)!
//
//        mapView.animate(to: camera)
//
//        print("fromlocation",fromlocation.latitude,fromlocation.longitude)
//        print("tolocation",tolocation.latitude,tolocation.longitude)
//
//        let placesClient = GMSPlacesClient()
//        placesClient.currentPlace { (placelikelihoods, error) in
//            if let err = error{
//                print("error in fetching places due to \(String(describing: error?.localizedDescription))")
//            }else{
//                if let likelyList = placelikelihoods{
//                    for likeliHood in likelyList.likelihoods{
//                        let place = likeliHood.place
//                        print("current place is \(place)")
//                        print(place.addressComponents!)
//                        print(place.formattedAddress!)
//                        print(place.name)
//                        print(place.placeID)
//
//                        self.pickUpTF.text = place.formattedAddress
//                        self.statestring = "pick"
//                    }
//                }
//                }
//            //}
////        }
//
//
//        self.locationManager.stopUpdatingLocation()
//        }
//    }
    
     func locationManager(_ manager: CLLocationManager, didFailWithError error: Error){
    
        print("unable to fetch location due to \(error.localizedDescription)")
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition){
        if ridenowtappedstatus == "no" {
        let latitude1 = mapView.camera.target.latitude
        let longitude1 = mapView.camera.target.longitude
            centerMapCoordinate = CLLocationCoordinate2D(latitude: latitude1 ,longitude:longitude1 )
        print(centerMapCoordinate.latitude)
        print(centerMapCoordinate.longitude)
        
            print("lat&lon",latitude1,longitude1)
            
        self.navigationController?.navigationBar.isHidden = false
        self.rideNowButton.isHidden = false
        self.rideLaterButton.isHidden = false
        fromView.isHidden = false
        toView.isHidden = false
        cabsView.isHidden = false
            self.driverview.isHidden = true
        //marker_ball.png  marker_ball_green.png
        pickUpBtn.isHidden = false
       
        if statestring == "pick" {
            fromlocation.latitude = latitude1
            fromlocation.longitude = longitude1
        
            self.getnearbyvehicles()

//            marker.position = fromlocation
//            marker.icon = UIImage(named: "marker_ball-1")!
//            marker.title = "SET PICKUP LOCATION"
//            marker.map = self.mapView
           // centerImageView.isHidden = true
        }
        else if statestring == "drop"
        {
            tolocation.latitude = latitude1
            tolocation.longitude = longitude1
             toView.frame = dummyView.frame
//
            fromView.isHidden = false
        
        }
       
        
        self.placeMarkerOnCenter(centerMapCoordinate: centerMapCoordinate)
    
    }
    }
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool){
        
       // self.fareView.isHidden = true
        
        self.cashView.isHidden = true
        if ridenowtappedstatus == "no" {
        rideNowButton.isHidden = true
        rideLaterButton.isHidden = true
        // fromView.isHidden = true
        toView.isHidden = true
        cabsView.isHidden = true
        pickUpBtn.isHidden = true
            
        self.driverview.isHidden = true
            
        if statestring == "pick" {
            toView.isHidden = true
            fromView.isHidden = false
           // mapView.clear()
            
        }
        else if statestring == "drop" {
         dummyView.frame = toView.frame
            toView.frame = fromView.frame
            fromView.isHidden = true
            toView.isHidden = false
        }
        
        self.navigationController?.navigationBar.isHidden = true
        
    }
    }

    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        if ridenowtappedstatus == "no" {
        centerImageView.isHidden = false
        
        let latitude = mapView.camera.target.latitude
        let longitude = mapView.camera.target.longitude
        centerMapCoordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        
      
       
    }
    }
     let marker = GMSMarker()
    let marker1 = GMSMarker()
   
    
    func placeMarkerOnCenter(centerMapCoordinate:CLLocationCoordinate2D) {
        
       // var markerimage = "marker_ball_green.png"
        self.view.addSubview(homeactivityIndicatorView)
        self.homeactivityIndicatorView.startAnimating()
        
        self.cabsView.isHidden = true
       self.rideNowButton.isHidden = true
        self.rideLaterButton.isHidden = true
        
        let ceo: CLGeocoder = CLGeocoder()
        
        
        let loc: CLLocation = CLLocation(latitude:centerMapCoordinate.latitude, longitude: centerMapCoordinate.longitude)
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                
                if placemarks != nil{
                    
                
                let pm = placemarks! as [CLPlacemark]
                
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    print("pm",pm)
                    print(pm.location)
                    print(pm.administrativeArea)
                    print(pm.country)
                    print(pm.locality)
                    print(pm.subLocality)
                    print(pm.thoroughfare)
                    print(pm.postalCode)
                    print(pm.subThoroughfare)
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    
                    print("Address",addressString)
                    if self.statestring == "pick" {
                        
                        self.pickUpTF.text = addressString
                        
                        print("pickupAddress",addressString)
                      if (pm.administrativeArea != nil)
                      {
                        self.pickupState = pm.administrativeArea! as NSString
                        }
                        if (pm.locality != nil)
                        {
                        self.pickupCity = pm.locality! as NSString
                        }
                        if (pm.postalCode != nil)
                        {
                        self.pickupZipCode = pm.postalCode! as NSString
                        }
                        if (pm.thoroughfare != nil)
                        {
                           // let subThoroughfare = pm.subThoroughfare! as NSString
                          //  let thoroughfare = pm.thoroughfare! as NSString
                            
                            self.pickupStreet = pm.thoroughfare! as NSString
                        }
//                        else
//                        {
//                            self.pickupStreet = pm.administrativeArea! as NSString
//                        }

                    }
                     else if self.statestring == "drop"
                    {
                    self.dropTF.text = addressString
                        
                        print("dropAddress",addressString)
                       // self.validDropLocation = true
                        
                        if(pm.thoroughfare != nil) {
                            
                       // let subThoroughfare = pm.subThoroughfare! as NSString
                            let thoroughfare = pm.thoroughfare! as NSString

                            print("dropstreet",thoroughfare,thoroughfare)
                            self.dropStreet = "\(thoroughfare)" + "\(thoroughfare)" as NSString
                        }
                       
                        if (pm.locality != nil){
                            
                            self.dropCity = pm.locality! as NSString
                        }
                        
                        if (pm.administrativeArea != nil){
                            
                            self.dropState = pm.administrativeArea! as NSString

                        }
                        if (pm.postalCode != nil){
                        
                        self.dropZipCode = pm.postalCode! as NSString
                        }
                        
                        print("dropTF",self.dropStreet,self.dropCity,self.dropState,self.dropZipCode)
                        
                    }
                  
                    self.homeactivityIndicatorView.stopAnimating()
                    
                    self.cabsView.isHidden = false
                    self.rideNowButton.isHidden = false
                    self.rideLaterButton.isHidden = false
                    
                    UserDefaults.standard.setValue(nil, forKey: "lat")
                    UserDefaults.standard.setValue(nil, forKey: "lng")
                    UserDefaults.standard.setValue(nil, forKey: "descriptiontxt")
                    
                }
                }else{
                    // placemarks is nil
                  //  self.displayAlertWithMessage(message: "Please check your internet connection ....!", title: "Oops...!")
                    
                    self.displayAlertWithMessage(message: "Please check your internet connection ....!", title: "Network Error", imagename: "error", buttonName: "OK")
                    
                    self.homeactivityIndicatorView.stopAnimating()
                    

                }
        })


    }

    func revealToggle(sender:SWRevealViewController)  {
        
    }
    
//    MARK: - API Methods
    
    func getCabsData()  {
        
        let headers = [
            "cache-control": "no-cache",
            "postman-token": "8806e182-18b3-071b-7816-84b90052460f"
        ]
        
        let request = NSMutableURLRequest(url: NSURL(string: "http://139.59.71.224:3010/api/Categories")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error!)
            } else {
                
                do {
                    
                    var json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves)
                    
                    print(json as! NSArray)
                }catch {
                    print(error.localizedDescription)
                    
                }
            }
        })
        
        dataTask.resume()
    }
    

    func postCustomerSupport() {
        
        let headers = [
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            "postman-token": "e1edc6b0-99f0-b8b6-467d-aca0bbc369e4"
        ]
        
        let postData = NSMutableData(data: "message=test message".data(using: String.Encoding.utf8)!)
        postData.append("&customerId=59f455eff97368461bc834dd".data(using: String.Encoding.utf8)!)
        
        print("postData",postData)
        
        let request = NSMutableURLRequest(url: NSURL(string: "http://139.59.71.224:3010/api/Supports")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error)
            } else {
                let httpResponse = response as? HTTPURLResponse
                print(httpResponse)
            }
        })
        
        dataTask.resume()
    }
    
    @IBAction func getTotalFareTapped(_ sender: Any) {
        
        mapView.clear()

        if (self.dropTF.text?.isEmpty)!
       {
        // self.present(self.gmscontroller1, animated: true, completion: nil)
        
        self.statustxt = "droptxt"
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchPlacesViewController")
        self.present(vc!, animated: true, completion: nil)
        }
        else
        {
            marker.icon = UIImage(named: "marker_ball-1")!
            marker.map = self.mapView
            marker.position = fromlocation

            marker1.icon = UIImage(named: "marker_ball")!
            marker1.map = self.mapView
            marker1.position = tolocation

            print("fromlocation",fromlocation.latitude,fromlocation.longitude)
            self.drawpath(startlocation: fromlocation, endlocation: tolocation)

            self.view.bringSubview(toFront: self.fareView)

            
           // self.fareView.bringSubview(toFront: self.fareActivityIndicatorView)
            
            self.fareView.isHidden = false

            self.fareActivityIndicatorView.startAnimating()
            
            self.fareActivityIndicatorView.isHidden = false
                self.carimageview.isHidden = true
                self.carNameLabel.isHidden = true
                self.carDistanceLabel.isHidden = true
                self.carTimeLabel.isHidden = true
                self.fareLabel.isHidden = true
            
            self.getTotalFareStatus = "gettotalFare"
            self.getDistance(origin:self.pickUpTF.text! as NSString,destination: self.dropTF.text! as NSString)
            
            
            
        }

       
//        if validDropLocation {
//            self.drawpath(startlocation: fromlocation, endlocation: tolocation)
//
//        }
//
//        if !validDropLocation {
//
//            self.geetingtotalfare()
//
//            //self.enterDropLocationButton.isHidden = false
//
//
//        }
    
    }
    
    
    func displayAlertWithMessage(message:String, title:String,imagename:String,buttonName:String) {
        
        
//        let alert = CDAlertView(title: title, message: message, type: .custom(image: UIImage(named:imagename)!))
//
//        let doneAction = CDAlertViewAction(title: buttonName)
//        alert.add(action: doneAction)
//
//        alert.show()
        
        let aletController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "ok", style: .default, handler: nil)
        aletController.addAction(okAction)
        self.present(aletController, animated: true, completion: nil)
    }
    
    @IBAction func confirmBookingTapped(_ sender: Any) {
        
        self.circleAnimation()
        
        self.confirmBookingView.isHidden = false
        
        self.fareView.isHidden = true
        
        self.conformbookingAPI()
        
        print(fromlocation)
        print(tolocation)
    }
    
    func drawpath(startlocation: CLLocationCoordinate2D,endlocation: CLLocationCoordinate2D) {
        
        
        let origin = "\(startlocation.latitude),\(startlocation.longitude)"
        let destination = "\(endlocation.latitude),\(endlocation.longitude)"
        
        print("origin",origin)
        print("destination",destination)
        
        let headers = [
            "cache-control": "no-cache",
            "postman-token": "2044dc93-420d-ba02-7079-188f8c870cd2"
        ]
        
        let request = NSMutableURLRequest(url: NSURL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error as Any)
            } else {
                let httpResponse = response as? HTTPURLResponse
                print(httpResponse as Any)
                
                print(data!)
                do
                {
                    let json = try JSONSerialization.jsonObject(with: data!, options: []) as! NSDictionary
                    print("Drawpath json is",json)
                    
                    let status = json.value(forKey: "status") as! String
                    
                    
                    if status == "ZERO_RESULTS"
                    {
                       // self.displayAlertWithMessage(message: "Please Enter Correct Pickup/Drop Point", title: "ZERO_RESULTS")
                        
                        //self.displayAlertWithMessage(message: "ZERO_RESULTS", title: "Error", imagename: "error", buttonName: "OK")
                        print("ZERO_RESULTS")

                    }
                    else if status == "OVER_QUERY_LIMIT"
                    {
                         //   self.displayAlertWithMessage(message: "Error from BackEnd", title: "Error", imagename: "error", buttonName: "OK")
                        
                        print("OVER_QUERY_LIMIT")
                    }
                    else
                    {
                        self.routes = json.object(forKey: "routes") as! NSArray
                        let dis = ((((self.routes.object(at: 0)as! NSDictionary).object(forKey: "legs")as! NSArray).object(at: 0)as! NSDictionary).object(forKey: "distance")as! NSDictionary).object(forKey: "value")as! CGFloat
                        self.distance = dis/1000
                        print(self.distance)
                        let totaltime = ((((self.routes.object(at: 0)as! NSDictionary).object(forKey: "legs")as! NSArray).object(at: 0)as! NSDictionary).object(forKey: "duration")as! NSDictionary).object(forKey: "value")as! CGFloat
                        self.time = totaltime/60
                        print(self.time)
                        
                        
                        
                        DispatchQueue.main.async {
                            
                            let routeoverviewpolyline = (self.routes.object(at: 0)as! NSDictionary).object(forKey: "overview_polyline")as! NSDictionary
                            let points = routeoverviewpolyline.object(forKey: "points")as! String
                            let path = GMSPath.init(fromEncodedPath: points)
                            self.polyline = GMSPolyline.init(path: path)
                            self.polyline.strokeWidth = 4
                            self.polyline.strokeColor = UIColor.black
                            self.polyline.map = self.mapView
                            
                            self.removePath = self.polyline
                            // to center the path of the travel
                            let gmscoordinateBounds = GMSCoordinateBounds.init(path: path!)
                            self.mapView.animate(with: GMSCameraUpdate.fit(gmscoordinateBounds, withPadding: 120))
                        }
                    }

                }
                catch
                {
                    print(error)
                }
                
            }
        })
        
        dataTask.resume()
        
        
        
    }
  
    func gettingtotalfare(distance:Int,duration:Int){
        
        self.drawpath(startlocation: fromlocation, endlocation: tolocation)

        
        let headers = [
            "cache-control": "no-cache",
            "postman-token": "f7de3b7c-efa6-8adf-7042-45616a78be31"
        ]
        

//        DispatchQueue.main.async {
//
//            self.carimageview.isHidden = true
//            self.carNameLabel.isHidden = true
//            self.carDistanceLabel.isHidden = true
//            self.carTimeLabel.isHidden = true
//            self.fareLabel.isHidden = true
//
//            self.fareActivityIndicatorView.startAnimating()
//        }
        
     //   let category = "TAC GO"
        
        let urlString = "http://139.59.71.224:3010/api/Categories/totalFare?details=%7B%20%22category%22%3A%22\(category!)%22%2C%20%22distance%22%3A%20\(distancevalue)%2C%20%22duration%22%3A%20\(durationvalue)%2C%20%22waitingTime%22%3A%200%20%7D"
        
        print("distance",distancevalue)
        print("duration",durationvalue)
        
        let url = urlString.replacingOccurrences(of: " ", with: "%20")
        
        print(urlString)
       let request = NSMutableURLRequest(url: NSURL(string: url)! as URL,cachePolicy: .useProtocolCachePolicy,timeoutInterval: 10.0)
        
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error as Any)
            } else {
                let httpResponse = response as? HTTPURLResponse
                print(httpResponse as Any)
                do{
                    let  json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as! NSDictionary
                    print("geetingtotalfare",json)
                    
                    DispatchQueue.main.async {
//                        

                        let dataDic = json.value(forKey: "data") as! NSDictionary
                        

                        if dataDic.value(forKey: "isSuccess") != nil
                        {
                            
                        print("data",dataDic)
                        
                            
                            if (self.getTotalFareStatus == "gettotalFare")
                            {
                            self.carimageview.isHidden = false
                            self.carNameLabel.isHidden = false
                            self.carDistanceLabel.isHidden = false
                            self.carTimeLabel.isHidden = false
                            self.fareLabel.isHidden = false
                            }
                            
                            
                            let urlstring = "http://139.59.71.224:3010/\(self.categoryimage!)"
                            
                            print("urlstring",urlstring)
                            let url : NSString = urlstring as NSString
                            let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
                            let searchURL : NSURL = NSURL(string: urlStr as String)!
                            print(searchURL)

                            let data = try? Data(contentsOf: searchURL as URL)
                            
                            self.carimageview.image = UIImage(data: data!)
                            
                            
                           // self.carImageview.image = UIImage(data: data!)
                            
                        self.carDistanceLabel.text = String(dataDic.value(forKeyPath: "distance") as! Double) + " km"
                        self.carTimeLabel.text = String(dataDic.value(forKeyPath: "duration") as! Double) + " mins"
                            self.carNameLabel.text = self.category! as String
                        self.fareLabel.text = String(dataDic.value(forKeyPath: "totalFareWithTax") as! Double) + " Rs"

                            self.fareActivityIndicatorView.stopAnimating()

                            self.fareActivityIndicatorView.isHidden = true
                        }
                        
                        else
                        {
                           // self.displayAlertWithMessage(message: dataDic.value(forKey: "message") as! String, title: "Error")
                            
                            self.displayAlertWithMessage(message: dataDic.value(forKey: "message") as! String, title: "Error", imagename: "error", buttonName: "OK")
                            
                            self.fareActivityIndicatorView.stopAnimating()
                            
                            self.fareActivityIndicatorView.isHidden = true
                        }
                    }
                   
                }
                catch{
                    
                }
            }
        })
        
        dataTask.resume()
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
       
    }
    
    
    func getnearbyvehicles(){


        let headers = [
            "cache-control": "no-cache",
            "postman-token": "05b55934-09d5-6a4c-980b-1e26daeca033"
        ]

        print("category",category,fromlocation.latitude,fromlocation.longitude)
      
       // let request = NSMutableURLRequest(url: NSURL(string: "http://139.59.71.224:3010/api/Vehicles/getNearByVehicles?details=%7B%20%22category%22%3A%22TAC%20GO%22%2C%20%22latitude%22%3A%2012.870738%2C%20%22longitude%22%3A%2077.613247%7D")! as URL,
        
        let urlstring = "http://139.59.71.224:3010/api/Vehicles/getNearByVehicles?details=%7B%20%22category%22%3A%22\(category!)%22%2C%20%22latitude%22%3A%20\(fromlocation.latitude)%2C%20%22longitude%22%3A%20\(fromlocation.longitude)%7D"
        
        print("urlstring",urlstring)
        
        let urlstr = urlstring.replacingOccurrences(of: " ", with: "%20")

        let request = NSMutableURLRequest(url: NSURL(string: urlstr)! as URL,cachePolicy: .useProtocolCachePolicy,timeoutInterval: 10.0)
        
        
//        let request = NSMutableURLRequest(url: NSURL(string: "http://139.59.71.224:3010/api/Vehicles/getNearByVehicles?details=%7B%20%22category\("TAC GO")latitude%22%3A%2012.870738%2C%20%22longitude%22%3A%2077.613247%7D")! as URL,
//                                          cachePolicy: .useProtocolCachePolicy,
//                                          timeoutInterval: 10.0)

        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers

        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error)
            } else {
                let httpResponse = response as? HTTPURLResponse
                print(httpResponse)
                
                do {
                    
                    
                    let result = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as! NSDictionary
                    print("getNearByVehiclesResult",result)
                    
              DispatchQueue.main.async {
                        
                if (((result.object(forKey: "data")as! NSDictionary).object(forKey: "isSuccess"))as! Int == 1)
                {
                    let locationArray = (result.object(forKey: "data")as! NSDictionary).object(forKey: "drivers")as! NSArray
                    
                    print("locationArray",locationArray)
                    let lat = ((locationArray.object(at: 0) as! NSDictionary).value(forKey: "location") as! NSDictionary).value(forKey: "lat")
                    
                    let lng = ((locationArray.object(at: 0) as! NSDictionary).value(forKey: "location") as! NSDictionary).value(forKey: "lng")
                    
                    print("lat,lng",lat!,lng!)
                    print("textfield",self.pickUpTF.text!)
                    
                    let destination = "\(String(describing: lat!)),\(String(describing: lng!))"
                    print("destination",destination)
                    
//                    if (self.pickUpTF.text?.isEmpty)!
//                    {
//                       // self.present(self.gmscontroller1, animated: true, completion: nil)
//
//                        let vc =  self.storyboard?.instantiateViewController(withIdentifier: "SearchPlacesViewController") as? SearchPlacesViewController
//                        vc?.state = "pickuptxt"
//                        self.navigationController?.pushViewController(vc!, animated: true)
//                    }
//                    else
//                    {
                        let originString = "\(self.self.fromlocation.latitude)" + "," + "\(self.fromlocation.longitude)"
                        
                        
                       // self.getDistance(origin: originString as NSString, destination:destination as NSString)
                        
                        self.getDistancebetweenwCabAndUser(origin: originString as NSString, destination: destination as NSString)
                 //   }
                    
                    for loc in locationArray{
                        
                        let lat = ((loc as! NSDictionary).object(forKey: "location")as! NSDictionary).object(forKey: "lat")!
                        print(lat)
                        let lon = ((loc as! NSDictionary).object(forKey: "location")as! NSDictionary).object(forKey: "lng")!
                        print(lon)
                        
                        let ann = GMSMarker()
                        
//                        var imageView = UIImageView(frame: CGSize(48, 48)); // set as you want
//                        let image = UIImage(named: "nearCab");
//                        imageView.image = image;
//                        self.view.addSubview(imageView);
                        
                        ann.icon = UIImage(named: "nearCab")
                                                
                       // ann.icon?.size = CGRect(48,48)

                        //  ann.icon = self.imageWithImage(image: UIImage(named: "carannotation")! scaledToSize: CGSize(width: 3.0, height: 3.0),
                     //   ann.icon = self.imageWithImage(image: UIImage(named: "imageName")!, scaledToSize: CGSize(width: 3.0, height: 3.0))

                        ann.position = CLLocationCoordinate2D(latitude: lat as! CLLocationDegrees, longitude: lon as! CLLocationDegrees)
                        ann.map = self.mapView
                        
                    }
                   
                        }
               else
               {
                
                let errorMessage = (result.object(forKey: "data")as! NSDictionary).object(forKey: "message")
                
                //self.displayAlertWithMessage(message: errorMessage as! String, title: "No Cabs")
                
               self.displayAlertWithMessage(message: errorMessage as! String, title: "No Drivers", imagename: "error", buttonName: "OK")
                
                        }
                    
                    }
                    
                }catch{
                    print("error \(error.localizedDescription)")
                }
                
            }
        })

        dataTask.resume()
    }
    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        //image.draw(in: CGRectMake(0, 0, newSize.width, newSize.height))
        image.draw(in: CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: newSize.width, height: newSize.height))  )
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    func getcategeory()  {
        

        let headers = [
        "cache-control": "no-cache",
        "postman-token": "1709579c-e1ef-ea84-30fb-ca07d72be3ca"
    ]
    //        let type = "TAC GO"
    //        var dis = 1.87
    //        var duration = 8.166
    
    //        let request = NSMutableURLRequest(url: NSURL(string: "http://139.59.71.224:3010/api/Categories/totalFare?details={category:\(type),distance:\(dis),duration: \(duration), waitingTime: \(dis)}")! as URL,
    //
    //                                          cachePolicy: .useProtocolCachePolicy,
    //                                          timeoutInterval: 10.0)
    //
    //        let request = NSMutableURLRequest(url: NSURL(string: "http://139.59.71.224:3010/api/Categories/totalFare?details=%7B%20%22category%22%3A%22\(type)%22%2C%20%22distance%22%3A%20\(dis)%2C%20%22duration%22%3A%20\(duration)%2C%20%22waitingTime%22%3A%200%20%7D")! as URL,
    //
    //                                          cachePolicy: .useProtocolCachePolicy,
    //                                          timeoutInterval: 10.0)
    //        let request = NSMutableURLRequest(url: NSURL(string: "http://139.59.71.224:3010/api/Vehicles/getNearByVehicles?details=%7B%20%22category\("TAC GO")latitude%22%3A%2012.870738%2C%20%22longitude%22%3A%2077.613247%7D")! as URL,
    //                                          cachePolicy: .useProtocolCachePolicy,
    //                                          timeoutInterval: 10.0)
    let request = NSMutableURLRequest(url: NSURL(string: "http://139.59.71.224:3010/api/Categories")! as URL,
                                      cachePolicy: .useProtocolCachePolicy,
                                      timeoutInterval: 10.0)
    
    request.httpMethod = "GET"
    request.allHTTPHeaderFields = headers
    
    let session = URLSession.shared
    let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
        if (error != nil) {
            print(error!)
        } else {
            let httpResponse = response as? HTTPURLResponse
            print(httpResponse!)
            do{
                let  json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as! NSArray
                print("categories",json)
                self.mainarray = json
                DispatchQueue.main.async {
                    
                    self.categeoryCollectionView.reloadData()
                }
            }
            catch{
                
            }
        }
    })
    
    dataTask.resume()
}
    func circleAnimation()
    {
        let circleCenter = CLLocationCoordinate2D(latitude: fromlocation.latitude, longitude: fromlocation.longitude)
        
        //        cirlce = GMSCircle(position: circleCenter, radius: 1000)
        //        cirlce.fillColor = UIColor(red: 0.0, green: 0.7, blue: 0, alpha: 0.1)
        //        cirlce.strokeColor = UIColor(red: 255/255, green: 153/255, blue: 51/255, alpha: 0.5)
        //
        //        cirlce.strokeColor = .blue
        //        cirlce.strokeWidth = 2
        //        cirlce.map = mapView
        
        m = GMSMarker(position: circleCenter)
        
        //custom marker image
        let pulseRingImg = UIImageView(frame: CGRect(x: -30, y: -30, width: 60, height: 60))
        pulseRingImg.image = UIImage(named: "circle-bk-line")
        pulseRingImg.isUserInteractionEnabled = false
        CATransaction.begin()
        CATransaction.setAnimationDuration(3.5)
        
        //transform scale animation
        var theAnimation: CABasicAnimation?
        theAnimation = CABasicAnimation(keyPath: "transform.scale.xy")
        theAnimation?.repeatCount = Float.infinity
        theAnimation?.autoreverses = false
        theAnimation?.fromValue = Float(0.0)
        theAnimation?.toValue = Float(5.0)
        theAnimation?.isRemovedOnCompletion = false
        
        pulseRingImg.layer.add(theAnimation!, forKey: "pulse")
        pulseRingImg.isUserInteractionEnabled = false
        CATransaction.setCompletionBlock({() -> Void in
            
            //alpha Animation for the image
            let animation = CAKeyframeAnimation(keyPath: "opacity")
            animation.duration = 5.5
            animation.repeatCount = Float.infinity
            animation.values = [Float(2.0), Float(0.0)]
            self.m.iconView?.layer.add(animation, forKey: "opacity")
        })
        
        CATransaction.commit()
        m.iconView = pulseRingImg
        m.layer.addSublayer(pulseRingImg.layer)
        m.map = mapView
        m.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        
    }
    
    func conformbookingAPI()
    {
       let pickupAdd = self.pickUpTF.text
        let dropAdd = self.dropTF.text
        
        print("pickup,dropAdd",pickupAdd!,dropAdd!)
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd hh:mm"
        let result = formatter.string(from: date)
        
        let randomNum:UInt32 = arc4random_uniform(100)
        let requestId:String = String(randomNum) //string works too
        
        var paramDic = Dictionary<String, Any>()
        
        var locationDic = Dictionary<String, Any>()
        locationDic = ["lat" :fromlocation.latitude, "lng" :fromlocation.longitude]
        
        var pickAddressDic = Dictionary<String, Any>()
        pickAddressDic = ["city" :pickupCity, "state" :pickupState,"latitude" :fromlocation.latitude, "longitude" :fromlocation.longitude,"zipCode" :pickupZipCode]
        
        var dropAddressDic = Dictionary<String, Any>()
        dropAddressDic = ["street" :dropStreet,"city" :dropCity, "state" :dropState,"latitude" :tolocation.latitude, "longitude" :tolocation.longitude,"zipCode" :dropZipCode]
        
        var customerDetailsDic = Dictionary<String, Any>()
        customerDetailsDic = ["name" :username, "email" :"","mobile" :mobileNo]
        
        paramDic = ["requestDate" :result,"requestId" :requestId, "location" :locationDic,"status" :"new", "totalPrice" :0,"pickAddress" :pickAddressDic,"dropAddress" :dropAddressDic, "customerDetails" :customerDetailsDic,"customerId" :Userid]
        
        print("paramDic",paramDic)
        
        
        var theJSONText = String()
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: paramDic,
            options: []) {
            theJSONText = String(data: theJSONData,
                                 encoding: .ascii)!
            print("JSON string = \(theJSONText)")
        }
        
        // create post request
        let url = URL(string: "http://139.59.71.224:3010/api/requests")!
        // let url = URL(string: urlString)!
        let jsonData = theJSONText.data(using: .utf8, allowLossyConversion: false)!
        
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = jsonData
        
        Alamofire.request(request).responseJSON {
            (response) in
            
            print("response",response)
            
            
            let driverDetails = response.value as! NSDictionary
            
            print("driverDetails",driverDetails)
            
            let driver = driverDetails.value(forKey: "drivers") as! NSArray
            
            self.driverName = (driver.object(at: 0) as! NSDictionary).value(forKey: "driverName") as! NSString

            self.timer = Timer.scheduledTimer(timeInterval: 40.0, target: self, selector: #selector(self.stopTimer), userInfo: nil, repeats: true)


        }
        
    }
    
    func stopTimer() {

            timer.invalidate()
        
        
        if UserDefaults.standard.value(forKey: "RideOk") != nil
        {
            UserDefaults.standard.setValue(nil, forKey: "RideOk")
            self.driverview.isHidden = false
            
            self.view.addSubview(self.driverview)
            
            self.carnostatusLabel.text = "TA 19"
            
            self.carNoLabel.text = "2424"
            
            self.vehicleNameLabel.text = self.category! as String
            
            self.driverNameLabel.text = self.driverName! as String
            
            print("self.driverNameLabel.text",self.driverNameLabel.text!)
            
            let carimage: UIImage = UIImage(named: "carimage")!
            self.carImageview = UIImageView(image: carimage)
            
            let driverimage: UIImage = UIImage(named: "driverimage")!
            self.driverImageView = UIImageView(image: driverimage)
        }
        else{
            
            displayAlertWithMessage(message: "Please try After Some Time", title: "No Drivers Found", imagename: "error", buttonName: "OK")
            
            
            
            self.backButton.isHidden = true
            self.menuButton.isHidden = false
            
            driverview.isHidden = true
            
            self.fareView.isHidden = true
            
            self.rideLaterView.isHidden = true
            
            if cancelmyview == "closeMyview"
            {
                self.cancelmyview = ""
                self.myView.isHidden = true
            }
            //self.myView.isHidden = true
            
            self.mapView.clear()
            let camera = GMSCameraPosition.camera(withLatitude: (fromlocation.latitude), longitude: (fromlocation.longitude), zoom: 15.0)
            
            centerImageView.isHidden = false
            self.view.bringSubview(toFront: centerImageView)
            mapView.animate(to: camera)
            statestring = "pick"
            ridenowtappedstatus = "no"
            self.view.sendSubview(toBack: confirmBookingView)
            // self.navigationItem.setLeftBarButton(menuButton, animated: true)
            self.menuButton.isHidden = false
            self.backButton.isHidden = true
            self.centerImageView.image = UIImage(named: "marker_ball-1")
            
        }
        self.m.map = nil


    }
    @IBAction func CallDriver(_ sender: Any) {
        
        
    }
    @IBAction func CancelRide(_ sender: Any) {
        
        driverview.isHidden = true
        
        self.mapView.clear()
        let camera = GMSCameraPosition.camera(withLatitude: (fromlocation.latitude), longitude: (fromlocation.longitude), zoom: 15.0)
        centerImageView.isHidden = false
        self.view.bringSubview(toFront: centerImageView)
        mapView.animate(to: camera)
        statestring = "pick"
        ridenowtappedstatus = "no"
        self.view.sendSubview(toBack: confirmBookingView)
        // self.navigationItem.setLeftBarButton(menuButton, animated: true)
        self.menuButton.isHidden = false
        self.backButton.isHidden = true
        self.centerImageView.image = UIImage(named: "marker_ball-1")
        
        
        self.m.map = nil
        
    }
    func timerAction()
    {
        
        //driverview.isHidden = true
    }
    func sendRideLaterNotificationToDriver(year:Int,month:Int,day:Int,hour:Int,minute:Int)
    
    {
        let urlString = "http://139.59.71.224:3010/api/Customers/sendRideLaterNotificationToDriver"

        var bodyDic = Dictionary<String, Any>()

        var locationDic = Dictionary<String, Any>()
        locationDic = ["lat" :fromlocation.latitude, "lng" :fromlocation.longitude]
        
        var pickAddressDic = Dictionary<String, Any>()
        pickAddressDic = ["street" :pickupStreet,"city" :pickupCity, "state" :pickupState,"latitude" :fromlocation.latitude, "longitude" :fromlocation.longitude,"zipCode" :pickupZipCode]
        
        var dropAddressDic = Dictionary<String, Any>()
        dropAddressDic = ["street" :dropStreet,"city" :dropCity, "state" :dropState,"latitude" :tolocation.latitude, "longitude" :tolocation.longitude,"zipCode" :dropZipCode]
        
        var requestDetails = Dictionary<String,Any>()
        requestDetails = ["requestDate":SelectedDate,"location":locationDic,"status":"new","totalPrice":0,"pickAddress":pickAddressDic,"dropAddress":dropAddressDic,"customerId": Userid]
        
        var details = Dictionary<String,Any>()

        details = ["year": year,
                   "month": month,
                   "day" : day,
                   "hour": hour,
                   "minute":minute,
                   "requestDetails":requestDetails]
        
        bodyDic = ["details": details]
            
        print("paramDic",bodyDic)
        var theJSONText = String()
        
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: bodyDic,options: []) {
            
            theJSONText = String(data: theJSONData,
                                 encoding: .ascii)!
            print("JSON string = \(theJSONText)")
        }
        
        // create post request
        let url = URL(string: urlString)!
        // let url = URL(string: urlString)!
        let jsonData = theJSONText.data(using: .utf8, allowLossyConversion: false)!
        
        var request = URLRequest(url: url)
        print("url",url)
        print("request",request)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = jsonData
        
        Alamofire.request(request).responseJSON {
            (response) in
            
            print("response",response)
            
            let data = Result.success("SUCCESS")
            
            print("data",data)
            if response.result.isSuccess {
                let resJson = response.result.value!
                
                print("resJson",resJson)
                
                let data = (resJson as AnyObject).value(forKey: "data")
                
                
                let message = (data as AnyObject).value(forKey: "message")
                
                print("data",data!,message!)
                
                //self.displayAlertWithMessage(message: message as! String, title: "Done")

               self.displayAlertWithMessage(message: message as! String, title: "SUCCESS", imagename: "success", buttonName: "DONE")
                
                self.m.map = nil
             
                //tlet var carImageview: UIImageView!
//                @IBOutlet var carnostatusLabel: UILabel!
//                @IBOutlet var carNoLabel: UILabel!
//                @IBOutlet var driverImageView: UIImageView!
            }
            if response.result.isFailure {
                let error : NSError = response.result.error! as NSError
                print(error)
            }
            
            }
        
    }
    
    
    func postdeviceToken()
    {
        let urlString = "http://139.59.71.224:3010/api/Customers/\(Userid!)"
        
        print("urlstring",urlString)
        
        //let theJSONText = UserDefaults.standard.value(forKey: "token") as! String

        var paramDic = Dictionary<String, Any>()
        
        paramDic = ["deviceId" : UserDefaults.standard.value(forKey: "token") as! String]
        
        print("paramDic",paramDic)
        var theJSONText = String()
        
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: paramDic,options: []) {

            theJSONText = String(data: theJSONData,
                                 encoding: .ascii)!
            print("JSON string = \(theJSONText)")
        }
        
        // create post request
         let url = URL(string: urlString)!
        let jsonData = theJSONText.data(using: .utf8, allowLossyConversion: false)!
        
        print("jsonData",jsonData)
        var request = URLRequest(url: url)
        print("url",url)
        print("request",request)
        request.httpMethod = HTTPMethod.patch.rawValue
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = jsonData
        
        Alamofire.request(request).responseJSON {
            (response) in
            
            print("postdeviceToken response",response)
            
            if response.result.isFailure {
                let error : NSError = response.result.error! as NSError
                print(error)
            }
            }
        
            
    }
   
}

