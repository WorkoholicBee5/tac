//
//  objects.swift
//  TAC
//
//  Created by Kshiteej deshpande on 13/12/17.
//  Copyright © 2017 Verizon. All rights reserved.
//

import UIKit

class objects: NSObject {
    
    var statustxt: String!
    var lat: NSNumber!
    var lng: NSNumber!
    var descriptiontxt: String = "desc"
    
    init(statustxt: String,lat: NSNumber,lng: NSNumber,descriptiontxt: String) {

        super.init()

        self.statustxt = statustxt
        self.lat = lat
        self.lng = lng
        self.descriptiontxt = descriptiontxt

    }
   
}
