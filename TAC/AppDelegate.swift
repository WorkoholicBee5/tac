//
//  AppDelegate.swift
//  TAC
//
//  Created by prk on 14/10/17.
//  Copyright © 2017 Diaprix. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import GooglePlacesSearchController
import UserNotifications
import Firebase
import FirebaseInstanceID
import FirebaseMessaging


@available(iOS 10.0, *)
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate{
    
    var window: UIWindow?
    
    var Details = NSDictionary()
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        // google maps
        //        AIzaSyAtyQ-tR-es6oIyqEV_veYsjnmPwUW7qzs
        GMSServices.provideAPIKey("AIzaSyDvnSkHURaHrAe8RaB0S4NYxrzOCZT-qpU")
        //        AIzaSyBhf3HZ7j2-UHym7YHKepF9gp-DfTwNM5Q
        //AIzaSyAE-ZXuM9HeNMNX3X44NxUDsxvHWZHi3v0
        GMSPlacesClient.provideAPIKey("AIzaSyDvnSkHURaHrAe8RaB0S4NYxrzOCZT-qpU")
        // GMSPlacesClient.provideAPIKey("AIzaSyAtyQ-tR-es6oIyqEV_veYsjnmPwUW7qzs")
        //        GMSServices.provideAPIKey("AIzaSyBhf3HZ7j2-UHym7YHKepF9gp-DfTwNM5Q")
        //        GMSPlacesClient.provideAPIKey("AIzaSyAtyQ-tR-es6oIyqEV_veYsjnmPwUW7qzs")
        //        GMSPlacesClient.provideAPIKey("AIzaSyBhf3HZ7j2-UHym7YHKepF9gp-DfTwNM5Q")
        //        GMSPlacesClient.provideAPIKey("YOUR_API_KEY")
        
        
        //create the notificationCenter
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
            // For iOS 10 data message (sent via FCM)
            //FIRMessaging.messaging().remoteMessageDelegate = self
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        FirebaseApp.configure()
        
        
        if UserDefaults.standard.value(forKey: "mobile") != nil
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let firstview = storyboard.instantiateViewController(withIdentifier: "FirstViewController") as! FirstViewController
            self.window?.rootViewController?.present(firstview, animated: true, completion: nil)
        }
        else
        {
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController")
            self.window?.rootViewController = initialViewController
            self.window?.makeKeyAndVisible()
            
        }
        
        return true
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
     
        if let refreshedToken = InstanceID.instanceID().token() {
           // print("InstanceID token: \(refreshedToken)")
            
            print("Registration succeeded! Token: ",refreshedToken)
            
            UserDefaults.standard.setValue(refreshedToken, forKey: "token")
            UserDefaults.standard.synchronize()
            

        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Registration failed!")
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    // Firebase notification received
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo:[AnyHashable : Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
//        if let messageID = userInfo[gcmMessageIDKey] {
//            print("Message ID: \(messageID)")
//        }
        
        // Print full message.
        print("userInfo",userInfo)
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        print("userInfo",userInfo)

      //   Print message ID.
        if let messageID = userInfo["gcm.notification.message"]
        {
            print("Message ID: \(messageID)")

            let responseData = (messageID as AnyObject).data(using: String.Encoding.utf8.rawValue)
        
            let driverDetails:NSDictionary = try! JSONSerialization.jsonObject(with: responseData!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary

            print("driverDetails",driverDetails)

            Details = driverDetails
            
            UserDefaults.standard.setValue(driverDetails, forKey: "driverDetails")
            UserDefaults.standard.synchronize()
            
//            let driverName = (jsonresult.value(forKey: "Driver") as! NSDictionary).value(forKey: "driverName") as! String
//
//            print("driverName",driverName)
            
            }
        
        print("Handle push from foreground\(userInfo)")
        
        let dict = userInfo["aps"] as! NSDictionary
        let d : [String : Any] = dict["alert"] as! [String : Any]
        let body : String = d["title"] as! String
        var title = NSString()
        print("body",body)
        title = "SUCCESS"

        if body == "Your Ride Has Cancelled"
        {
            title = "Canceled"
            
            print("body",body)
        }
        print("Title:\(title) + body:\(body)")
        self.showAlertAppDelegate(title: title as String,message:body,buttonTitle:"ok",window:self.window!)

            

        completionHandler(UIBackgroundFetchResult.newData)
}
   
//    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
//
//        print("Handle push from foreground\(notification.request.content.userInfo)")
//
//        let dict = notification.request.content.userInfo["aps"] as! NSDictionary
//        let d : [String : Any] = dict["alert"] as! [String : Any]
//        let body : String = d["title"] as! String
//        var title = NSString()
//
//        if body == "Your Ride Has Cancelled"
//        {
//             title = "Canceled"
//
//        }
//        title = "SUCCESS"
//        print("Title:\(title) + body:\(body)")
//        self.showAlertAppDelegate(title: title as String,message:body,buttonTitle:"ok",window:self.window!)
//
//    }
    
    func showAlertAppDelegate(title: String,message : String,buttonTitle: String,window: UIWindow){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: UIAlertActionStyle.default, handler: nil))
        window.rootViewController?.present(alert, animated: false, completion: nil)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            UIAlertAction in
            print("OK Pressed")
            
            UserDefaults.standard.setValue("RideAccepted", forKey: "RideOk")
            
        }
        
        alert.addAction(okAction)

    }
    
}

