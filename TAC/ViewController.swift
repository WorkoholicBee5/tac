//
//  ViewController.swift
//  TAC
//
//  Created by prk on 14/10/17.
//  Copyright © 2017 Diaprix. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var toggleButton: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        
        
        let reveal = self.revealViewController()
        if (reveal != nil) {
            
            self.toggleButton.target = self.revealViewController()
            self.toggleButton.action = #selector(self.revealToggle)
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
            
            
            
        }
    }

    func revealToggle()  {
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func onLeftBarButtonTapped(_ sender: UIButton) {
        
        
        
    }
}

