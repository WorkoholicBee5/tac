//
//  YourRidesDetailsViewController.swift
//  TAC
//
//  Created by Kshiteej deshpande on 07/12/17.
//  Copyright © 2017 Verizon. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Alamofire

class YourRidesDetailsViewController: UIViewController,CLLocationManagerDelegate,GMSMapViewDelegate,UITableViewDataSource {

    @IBOutlet var myView: UIView!
    @IBOutlet var ridesDetailsTableview: UITableView!
    var  mapView = GMSMapView()
    
    var driverimageUrlStr : NSString!
    var driverNameStr : NSString!
    var Status : NSString!
    
    var cabimageStr : NSString!
    var cabName : NSString!
    
    var priceStr : NSNumber!
    
    var originStr : NSString!
    var destinationStr : NSString!
    
    var pickuplatStr : NSNumber!
    var pickuplngstr : NSNumber!
    
    var droplatStr : NSNumber!
    var droplngStr : NSNumber!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
       // ridesDetailsTableview.estimatedRowHeight = 150
       // ridesDetailsTableview.rowHeight = UITableViewAutomaticDimension
        
        mapView  = GMSMapView(frame: CGRect(x: 0, y:0 , width: self.view.frame.size.width, height: self.myView.frame.size.height))
        
        let mapInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 10.0, right: 0.0)
        mapView.padding = mapInsets
        
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        mapView.delegate = self
        
        
        let pickupmarker = GMSMarker()
        pickupmarker.position = CLLocationCoordinate2D(latitude: pickuplatStr as! CLLocationDegrees, longitude: pickuplngstr as! CLLocationDegrees)
        print("pickuplatStr",pickuplatStr,pickuplngstr)
        pickupmarker.icon = UIImage(named: "marker_ball-1")
        pickupmarker.map = mapView
        
        
        let dropmarker = GMSMarker()
        dropmarker.position = CLLocationCoordinate2D(latitude: droplatStr as! CLLocationDegrees, longitude: droplngStr as! CLLocationDegrees)
        
        print("droplatStr",droplatStr,droplngStr)
        dropmarker.icon = UIImage(named: "marker_ball")
        dropmarker.map = mapView
        
        let cameraPosition = GMSCameraPosition(target: CLLocationCoordinate2D(latitude: pickuplatStr as! CLLocationDegrees, longitude: pickuplngstr as! CLLocationDegrees), zoom: 10, bearing: 3, viewingAngle: 3)
        mapView.camera = cameraPosition
        
        self.myView.addSubview(mapView)
    }

    @IBAction func backButton(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    func numberOfSections(in tableView: UITableView) -> Int // Default is 1 if not implemented
    {
        return 1;
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
    
        return 4;

    }
  //  @available(iOS 2.0, *)
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
         if indexPath.row == 0 {
            
           
            let cell = tableView.dequeueReusableCell(withIdentifier: "DriverProfileTableViewCell") as! DriverProfileTableViewCell
            
            //set the data here
            
            cell.driverNameLabel.text = driverNameStr as String?
          //  cell.driverImageView.image = UIImageView
            
            cell.statusImageView.image =  UIImage(named:"success-rubber-stamp")

            if Status == "canceled"
            {
                cell.statusImageView.image =  UIImage(named:"cancelled-red-stamp")
            }
            
            
            return cell
        }
        else if indexPath.row == 1 {
            
             let cell = tableView.dequeueReusableCell(withIdentifier: "CabCategoryTableViewCell") as! CabCategoryTableViewCell
            //set the data here
            
            cell.cabNameLabel.text = cabName as String?
            //cell.cabImageView
            
            return cell
        }
        else if indexPath.row == 2 {
            
           let cell = tableView.dequeueReusableCell(withIdentifier: "PriceTableViewCell") as! PriceTableViewCell
            //set the data here
            cell.priceLabel.text = "\(priceStr!)"
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DistanceTableViewCell") as! DistanceTableViewCell
            //set the data here
            
            cell.originLabel.text = originStr as String?
            cell.destinationLabel.text = destinationStr as String?
            
            return cell
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        
        return 200;
//        if indexPath.row == 0 {
//
//            return 100.0;//Choose your custom row height
//
//         //   return UITableViewAutomaticDimension
//
//        }
//        else if indexPath.row == 1
//        {
//            return 80.0;//Choose your custom row height
//
//          //  return UITableViewAutomaticDimension
//
//        }
//        else if indexPath.row == 2
//        {
//            return 80.0;//Choose your custom row height
//
//           // return UITableViewAutomaticDimension
//
//        }
//        else
//        {
//            return 120.0;//Choose your custom row height
//
//           // return UITableViewAutomaticDimension
//
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
