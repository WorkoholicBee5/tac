//
//  RideSafetyViewController.swift
//  TAC
//
//  Created by prk on 29/10/17.
//  Copyright © 2017 Diaprix. All rights reserved.
//

import UIKit

class RideSafetyViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextTapped(_ sender: Any) {
//        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dummy"), object: nil, userInfo: ["class":"RideSafetyViewController", "direction": "next"])
        self.dismiss(animated: true, completion: nil)
        
       // let vc = self.storyboard?.instantiateViewController(withIdentifier: "FirstViewController")
       // self.present(vc!, animated: true, completion: nil)
        
    }

    @IBAction func onPrevTapped(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dummy"), object: nil, userInfo: ["class":"RideSafetyViewController", "direction": "prev"])
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
